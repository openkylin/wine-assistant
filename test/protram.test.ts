// basic.spec.ts
// organizing tests

import { describe, expect, test, expectTypeOf } from "vitest";

import { ProgramManager } from "../main/managers/ProgramManager";
import { DependencyManager } from "../main/managers/DependencyManager";

const instance = ProgramManager.getInstance();
const depInstance = DependencyManager.getInstance();

describe("person", () => {
  test("getDepends", async () => {
    await expectTypeOf(
      depInstance.getDependencies(["dotnet462"])
    ).resolves.toBeArray();
    /*
    depInstance.getDependencies(["dotnet462"]).then(
      (ds) => {
        console.log(ds);
      },
      (reason) => {
        console.log(reason);
      }
    );
    */
  });
});
