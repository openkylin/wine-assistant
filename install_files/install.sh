#!/usr/bin/bash

###
### Usage: 
###     sudo ./install.sh
###

#set -x

VERSION="1.0"

# 界面/窗口相关设置
RET=0
# 正常安装流程界面大小
WIN_WIDTH=720
WIN_HEIGHT=800
# 提示框大小，警告/错误
ERR_WIDTH=190
ERR_HEIGHT=130
WORKPATH=$(dirname $0)
LICENSE="$WORKPATH/License"
PRODUCT_ICON="$WORKPATH/wine-assistant.png"
PRODUCT_NAME="wine-助手"

# Set Bash color
ECHO_PREFIX_INFO="\033[1;32;40mINFO: \033[0;0m"
ECHO_PREFIX_ERROR="\033[1;31;40mError: \033[0;0m"

# Try command  for test command result.
function try_command {
    "$@"
    status=$?
    if [ $status -ne 0 ]; then
        echo -e $ECHO_PREFIX_ERROR "命令执行错误[$@], 错误码 $status."
        exit $status
    fi
    return $status
}

# Try command.
function try_command_zenity {
    str=$($@ 2>&1)
    status=$?
    echo $status
    if [ $status -ne 0 ]; then
        echo -e $str
        zenity --error \
            --text="安装失败" \
            --width=$ERR_WIDTH \
            --height=$ERR_HEIGHT >/dev/null 2>&1
    
        exit $status
    fi
    return $status
}

ret=0
command -v zenity >/dev/null 2>&1 || { ret=1; }

if [ $ret -ne 0 ]; then
  pkexec apt-get install zenity
fi

export SUDO_ASKPASS=./askpass.sh
if [[ $EUID -ne 0 ]]; then
    echo -e $ECHO_PREFIX_ERROR "安装需要sudo权限！" 1>&2
    sudo -A ./install.sh
    exit 1
fi

zenity_install() {
    str=$(command -v zenity)
    if [[ "$str" == "" ]]; then
        try_command sudo -A apt install -y zenity > /dev/null
    fi
    try_command zenity --help >/dev/null 2>&1
}


quick_install() {
    (
        pushd $WORKPATH
        PROGRESS=1
        echo $PROGRESS
        echo "# 安装 wine-runtime..."
        try_command_zenity sudo -A apt install -y ./wine-runtime_*.deb
        
        PROGRESS=20
        echo $PROGRESS
        echo "# 安装 wine..."
        try_command_zenity sudo -A apt install -y ./wine_*.deb

        PROGRESS=40
        echo $PROGRESS
        echo "# 安装 wine-mono-gecko..."
        try_command_zenity sudo -A apt install -y ./wine-mono-gecko_*.deb

        PROGRESS=60
        echo $PROGRESS
        echo "# 安装 okylin-wine..."
        try_command_zenity sudo -A apt install -y ./okylin-wine_*.deb
        
        PROGRESS=80
        echo $PROGRESS
        echo "# 安装 wine-assistant..."
        try_command_zenity sudo -A apt install -y ./wine-assistant_*.deb

        
        PROGRESS=100
        echo $PROGRESS
        echo "# 安装结束。"
        
        popd
    ) | zenity --progress \
        --title="$PRODUCT_NAME 安装" \
        --no-cancel \
        --text="开始安装..." \
        --percentage=0 \
        --width=$WIN_WIDTH \
        --height=100 \
        --window-icon=$PRODUCT_ICON \
        --ok-label="完成" >/dev/null 2>&1
}


interface() {
    zenity --text-info \
        --filename=$LICENSE \
        --title=$PRODUCT_NAME \
        --width=$WIN_WIDTH \
        --height=$WIN_HEIGHT \
        --window-icon=$PRODUCT_ICON \
        --checkbox="已阅读和同意上述的服务和隐私协议!"\
        --cancel-label="取消" \
        --ok-label="安装软件" >/dev/null 2>&1
    
    if [ $? -ne 0 ]; then
        exit
    fi
}

help() {
    sed -rn 's/^### ?//;T;p' "$0"
}

if [ $# -eq 0 ]; then
    xhost +
    zenity_install
    if [ $? -eq 0 ]
    then
        interface
        quick_install
    fi
else
    help
fi
