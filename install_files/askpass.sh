#!/usr/bin/bash
#!/bin/sh

ENTRY=`zenity --width=300 --title="sudo权限" --entry --window-icon=./wine-assistant.png --text="安装Wine助手所需依赖需要sudo权限\n请输入密码:" --hide-text --cancel-label="取消" --ok-label="确认"`

case $? in
  0)
	 	echo "`echo $ENTRY | cut -d'|' -f2`"
	;;
 	1)
		echo "";;
  -1)
		echo "";;
esac
