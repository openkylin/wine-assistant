/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { DependencyRun, InfoBase } from "../commons/define";
import { DependencyManager } from "./DependencyManager";
import { depInfo } from "../commons/define";
import { logger } from "../utils/Logger";

class TreeItem {
  Key: string = "";
  Parent: TreeItem | undefined;
  Level: number = 1;
  HardDeps?: string[];
  licenseUrl: string = ""; //or run?
  Children: { [key: string]: TreeItem } = {};
  Version: string = "";
  Prev: string | undefined = "";
}

export class RelationManager {
  //保存一个树林，key为root，value为树杈
  private _dependTree: {
    [key: string]: /**mainKey*/ {
      [key: string]: /**依赖key */ TreeItem /**用来记录树形关系 */;
    };
  } = {};
  private _dependQueue: { [key: string]: depInfo[] } = {};
  private static _instance: RelationManager | undefined;
  private constructor() {}
  public static getInstance(): RelationManager {
    if (this._instance === undefined) {
      this._instance = new RelationManager();
    }
    return this._instance;
  }

  private recursiveTree(mainKey: string, key: string, prev?: string) {
    key = key + prev;

    let tree = this._dependTree[mainKey];
    if (tree == undefined) {
      return;
    }
    let treeItem = tree[key];
    if (Object.entries(treeItem.Children).length > 0) {
      return;
    }
    //console.log(treeItem.Key, treeItem.Level);
    delete tree[key];
    if (key !== mainKey) {
      this._dependQueue[mainKey].push({
        Key: treeItem.Key,
        Level: treeItem.Level,
        licenseUrl: treeItem.licenseUrl,
        HardDep: treeItem.HardDeps || [],
        Version: treeItem.Version,
      });
      //this.appendRelation(mainKey, key);
    }
    if (treeItem.Parent !== undefined) {
      delete treeItem.Parent.Children[key];
      if (Object.entries(treeItem.Parent.Children).length == 0) {
        this.recursiveTree(mainKey, treeItem.Parent.Key, treeItem.Prev);
      }
    }
  }

  /**
   * 递归查询依赖信息
   * @param mainKey 主key，用来记录关系
   * @param base
   * @param callback 放给调用方的接口，每一次递归，都会将当前查询到的依赖信息传递到callback里
   * @returns
   */
  public async recursiveDependencies(
    mainKey: string,
    base: InfoBase,
    level: number,
    finished: (deps: depInfo[], err: string) => void,
    err: (reason: string) => void,
    prev?: string
    //callback?: (depRuns: DependencyRun[]) => void
  ) {
    if (prev === undefined) {
      prev = "";
    }
    const depManager = DependencyManager.getInstance();
    //首次进来，给mainkey初始化一个树形结构
    if (this._dependTree[mainKey] == undefined) {
      this._dependTree[mainKey] = {};
      this._dependQueue[mainKey] = [];
      //this._treeLevel[mainKey] = 0;
    }

    //初始化当前节点的item
    let currItem = this._dependTree[mainKey][base.Key + prev];
    if (currItem == undefined) {
      currItem = new TreeItem();
      currItem.Level = level;
      currItem.Key = base.Key;
      currItem.Prev = prev;
      //保存到树形结构里
      this._dependTree[mainKey][base.Key] = currItem;
    }

    //查询依赖分类
    depManager.getDependencies(base.Dependencies).then((deps) => {
      //ps保存当前base节点的子节点，用于并发查询
      const ps = [] as Promise<DependencyRun>[];
      let notFoundDeps = [] as string[];
      base.Dependencies.forEach((d) => {
        let idx = deps.findIndex((val) => {
          return val.Key == d;
        });
        if (idx < 0) {
          notFoundDeps.push(d);
        }
      });
      if (notFoundDeps.length > 0) {
        err(" depends not found：" + notFoundDeps.join(","));
        delete this._dependTree[mainKey];
        delete this._dependQueue[mainKey];
        return;
      }
      /*
      if (deps.length !== base.Dependencies.length) {
        err("Recursive dependencies fail");
        delete this._dependTree[mainKey];
        return;
      }
      */
      deps.forEach((val) => {
        //增加防止循环依赖的逻辑，核心思想，依赖不存在才进行下一次递归
        /**
         * 场景：
         * a依赖b，b依赖c,c依赖b和d
         * 第一次递归a依赖b，检查b不存在继续下一次递归
         * 第二次递归b依赖c，检查c不存在继续下一次递归
         * 第三次递归c依赖b和d，检查b和d,b存在，d不存在，则d进入下次递归
         */
        if (this._dependTree[mainKey][val.Key] === undefined) {
          ps.push(depManager.getDependencyRun(val.Category, val.Key));
        }
      });

      //如果当前base节点已经没有子节点了
      if (ps.length == 0) {
        //递归从树形结构中减去当前base节点
        //思路：从树中减去当前base节点，并递归到父节点，查看父节点如果也变为叶子节点，那么将父节点也减掉，依次递归直到将所有节点减去，表示整个递归逻辑调用完毕，将递归得到的依赖列表返回
        this.recursiveTree(mainKey, base.Key, prev);
        if (Object.entries(this._dependTree[mainKey]).length == 0) {
          delete this._dependTree[mainKey];
          if (finished) {
            let result = this._dependQueue[mainKey] ?? [];
            //排个序，把最深的节点放在前面
            result.sort((a, b): number => {
              return b.Level - a.Level;
            });
            finished(result, "");
          }
        }
        return;
      }
      //进入下一轮递归，递归查询子节点
      Promise.all(ps)
        .then((runs) => {
          //将子节点添加到树中
          runs.forEach((run) => {
            const childItem = new TreeItem();
            childItem.Level = level + 1;
            childItem.Parent = currItem;
            childItem.Prev = prev;
            childItem.Key = run.Key;
            childItem.licenseUrl = run.License_url;
            childItem.HardDeps = run.Hard_Dependencies || [];
            childItem.Version = run.Version;
            currItem.Children[run.Key + base.Key] = childItem;

            this._dependTree[mainKey][run.Key + base.Key] = childItem;
          });
          //循环递归调用
          runs.forEach((run) => {
            this.recursiveDependencies(
              mainKey,
              run,
              level + 1,
              finished,
              err,
              base.Key
            );
          });
        })
        .catch((reason) => {
          if (finished) {
            finished([], reason);
          }
        });
    });
  }
}
