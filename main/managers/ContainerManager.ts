/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { spawn, exec } from "child_process";
import path from "path";
import { IpcProcess } from "../commons/define";
import { WorkDirConfig } from "../commons/config";
import { loadConfig, saveConfig, fileExists } from "../utils/Utils";
import { logger } from "../utils/Logger";
import fs from "fs-extra";
import * as iconv from "iconv-lite";
import { ipcRenderer } from "electron";
import { StatusManager } from "./StatusManager";
import { WineCommand } from "../utils/WineCommand";
import { dialog } from "electron";
import { ExportIcon } from "../utils/IconUtil";
import { generateShortcut } from "../utils/Utils";
import { execSync } from "child_process";
import * as yaml from "yaml";

export class ContainerManager extends IpcProcess {
  private static instance: ContainerManager;
  private static _containerStatus = {
    importBackup: false,
    createBackup: false,
    kylinWineBuild: false,
  };
  private _deriveDebConfig = {};
  private static wine: string[] = [];
  private constructor() {
    super();
  }

  public static getInstance(): ContainerManager {
    if (!this.instance) {
      this.instance = new ContainerManager();
    }
    return this.instance;
  }

  private _buildScript = path.join(
    WorkDirConfig.WorkDir,
    "kylin-wine-bottle",
    "wine-assistant",
    "main.pyc"
  );
  private _buildConfig = path.join(
    WorkDirConfig.WorkDir,
    "kylin-wine-bottle",
    "wine-assistant",
    "default.json"
  );

  private _dllList = path.join(WorkDirConfig.ConfigDir, "winfixdll.json");

  public debuildEnvironment(): Promise<any> {
    return new Promise<any>((res, _rej) => {
      const fs = require("fs");
      try {
        fs.accessSync(this._buildScript, fs.constants.F_OK);
      } catch (err) {
        res(false);
        return;
      }
      res(true);
    });
  }

  public async getDebuildConfig(): Promise<any> {
    if (Object.keys(this._deriveDebConfig).length === 0) {
      let config = JSON.parse(loadConfig(this._buildConfig));
      Object.assign(this._deriveDebConfig, config);
      this._deriveDebConfig["Log"] = this._deriveDebConfig["Log"].join("\n");
      if (ContainerManager.wine.length === 2) {
        this._deriveDebConfig[
          "Depends"
        ] = `${ContainerManager.wine[0]}(>=${ContainerManager.wine[1]})`;
      }
    }
    let info = await this.getFirstSoftwareInfo();
    Object.assign(this._deriveDebConfig, info);
    if (this._deriveDebConfig["Env"].hasOwnProperty("WINEPREFIX")) {
      delete this._deriveDebConfig["Env"]["WINEPREFIX"];
    }
    return this._deriveDebConfig;
  }

  public kylinWineDebuild(data): Promise<any> {
    return new Promise<any>((res, _rej) => {
      const fs = require("fs");
      let targetDir = data.config.buildPath;
      let targetConf = path.join(
        targetDir,
        data["config"]["Package"] + ".json"
      );
      try {
        if (!fs.existsSync(targetDir)) {
          fs.mkdirSync(targetDir);
        }
      } catch (err) {}
      data["config"]["Env"]["WINEPREFIX"] = path.join(
        WorkDirConfig.ContainerDir,
        data.bottle
      );
      saveConfig(targetConf, JSON.stringify(data["config"]));
      let cmd =
        'mate-terminal --window --command "python3 ' +
        this._buildScript +
        " debuild -f " +
        targetConf +
        '"';
      logger.debug(`Running command: ${cmd}`);
      let stdios = [null, null, null] as any[];
      const proc = spawn(cmd, {
        shell: true,
        stdio: stdios,
      });
      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Command failed with code ${code}`);
          res(code);
          //proc.kill();
        } else {
          logger.debug(`Command with code ${code}`);
          res(0);
        }
      });
    });
  }
  // data {
  //   path："",
  //   bottle: "",
  // }
  public createBackup(data): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let now = new Date();
      let year = now.getFullYear(); // 年份
      let month = now.getMonth() + 1; // 月份（从0开始计数，所以要加1）
      let day = now.getDate(); // 日
      let hours = now.getHours(); // 小时
      let minutes = now.getMinutes(); // 分钟
      let seconds = now.getSeconds(); // 秒
      let formattedDate = `${year}${month.toString().padStart(2, "0")}${day
        .toString()
        .padStart(2, "0")}${hours.toString().padStart(2, "0")}${minutes
        .toString()
        .padStart(2, "0")}${seconds.toString().padStart(2, "0")}`;
      logger.info(formattedDate); // 输出类似 "2018-10-15 14:45:00" 的字符串
      let targetDirName =
        "WineAssistantBackup_" + data.bottle + "_" + formattedDate;
      let targetDir = path.join(data.path, targetDirName);
      let regex = new RegExp(WorkDirConfig.HomeDir, "g");

      try {
        if (!fs.existsSync(targetDir)) {
          fs.mkdirSync(targetDir);
        }
      } catch (err) {
        res(err);
        return;
      }

      if (fs.existsSync(WorkDirConfig.IconsDir)) {
        fs.copySync(WorkDirConfig.IconsDir, path.join(targetDir, "icons"));
      } else {
        fs.mkdirSync(path.join(targetDir, "icons"));
      }
      logger.debug(`backup icons over`);

      let installInfo = StatusManager.getInstance().getInstallInfos();
      let installConf = JSON.stringify(
        installInfo,
        (key: string, value: any) => {
          if (key === "InstallStatus" && value === "Installing") {
            return "Uninstall";
          }
          return value;
        }
      ).replace(regex, "%HOME_PATH%");
      saveConfig(path.join(targetDir, "install_status.json"), installConf);
      logger.debug(targetDir);
      logger.debug(
        JSON.stringify(installInfo, (key: string, value: any) => {
          if (key === "InstallStatus" && value === "Installing") {
            return "Uninstall";
          }
          return value;
        })
      );
      logger.debug(`backup install config over`);

      let bottleConf = JSON.stringify(
        StatusManager.getInstance().getInstalledSoftInfo()
      );
      saveConfig(
        path.join(targetDir, data.bottle + ".json"),
        bottleConf.replace(regex, "%HOME_PATH%")
      );
      logger.debug(`backup bottle config over`);

      if (!fs.existsSync(path.join(WorkDirConfig.ContainerDir, data.bottle))) {
        res(1);
        return;
      }

      let cmd = `cd ${data.path} && cp -rf ${path.join(
        WorkDirConfig.ContainerDir,
        data.bottle
      )} ${targetDirName} && mksquashfs "${targetDirName}" "${targetDirName}.sqfs" -comp xz && rm -rf "${targetDirName}"`;
      logger.debug(`Running command: ${cmd}`);

      let stdios = [null, null, null] as any[];
      const proc = spawn(cmd, {
        shell: true,
        stdio: stdios,
      });
      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Command failed with code ${code}`);
          res(code);
          //proc.kill();
        } else {
          logger.debug(`Command with code ${code}`);
          res(0);
        }
      });
    });
  }

  public replaceBackup(unzipPath: string, bottle: string): string {
    let regex = new RegExp("%HOME_PATH%", "g");
    let backupBottle = path.join(unzipPath, bottle);
    let backupIcon = path.join(unzipPath, "icons");
    let backupInstallConfig = loadConfig(
      path.join(unzipPath, "install_status.json")
    ).replace(regex, WorkDirConfig.HomeDir);
    let backupBottleConfig = loadConfig(
      path.join(unzipPath, bottle + ".json")
    ).replace(regex, WorkDirConfig.HomeDir);
    logger.info("backupInstallConfig", backupInstallConfig);
    logger.info("backupBottleConfig", backupBottleConfig);

    let winePrefix = path.join(WorkDirConfig.ContainerDir, bottle);
    let icon = path.join(WorkDirConfig.WorkDir, "icons");
    let installConfig = path.join(
      WorkDirConfig.ConfigDir,
      "install_status.json"
    );
    let bottleConfig = path.join(WorkDirConfig.ConfigDir, bottle + ".json");

    if (
      !fs.existsSync(backupBottle) &&
      !fs.existsSync(backupInstallConfig) &&
      backupInstallConfig === "" &&
      backupBottleConfig === ""
    ) {
      return "解压文件错误";
    }

    if (fs.existsSync(icon)) {
      fs.removeSync(icon);
    }
    if (fs.existsSync(backupIcon)) {
      fs.moveSync(backupIcon, icon);
    } else {
      fs.mkdirSync(icon);
    }

    saveConfig(installConfig, backupInstallConfig);

    saveConfig(bottleConfig, backupBottleConfig);
    StatusManager.getInstance().reloadInstallStatusConfig();
    StatusManager.getInstance().reloadInstallSoftInfo();

    fs.moveSync(backupBottle, winePrefix, { overwrite: true });

    return "";
  }

  public restartApp() {
    ipcRenderer.send("restart-app");
  }

  // data {
  //   path："",
  //   bottle: "",
  // }
  public importBackup(data: any): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let unzipPath = path.join(WorkDirConfig.WorkDir, `${data.bottle}_backup`);

      if (fs.existsSync(unzipPath)) {
        fs.removeSync(unzipPath);
      }

      let cmd = `unsquashfs -no-xattrs -d "${unzipPath}" "${data.path}"`;
      logger.debug(`Running command: ${cmd}`);

      let stdios = [null, null, null] as any[];
      const proc = spawn(cmd, {
        shell: true,
        stdio: stdios,
      });
      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Command failed with code ${code}`);
          res("解压失败");
          return;
          //proc.kill();
        }

        logger.debug(`Command with code ${code}`);
        if (!fs.existsSync(unzipPath)) {
          res("解压失败");
          return;
        }
        res(this.replaceBackup(unzipPath, data.bottle));
      });
    });
  }

  public startExe(data): Promise<any> {
    // data = {
    //   exec: "regedit#winecfg#/path/to/execFile",
    //   env: {},
    //   args: ""|undefined,
    //   bottle: ""
    // };
    return new Promise<any>((res, rej) => {
      if (data["env"]["WINEDEBUG"] === undefined) {
        data["env"]["WINEDEBUG"] = "";
      }

      const wineCmd = new WineCommand(
        data.run,
        data["env"],
        data["args"],
        data["bottle"],
        true
      );
      const [errorMsg, cmd] = wineCmd.buildCommand();
      if (errorMsg !== "") {
        rej(errorMsg);
        return;
      }

      let logfile = path.join(
        WorkDirConfig.WorkDir,
        "logs",
        "WineAssistantRun.log"
      );
      if (data.logfile) {
        logfile = data.logfile;
      }

      if (!fs.existsSync(path.dirname(logfile))) {
        fs.mkdirpSync(path.dirname(logfile));
      }

      // console.log(cmd + " >" + logfile + " 2>&1");
      const proc = spawn(cmd + " >" + logfile + " 2>&1", {
        shell: true,
      });

      // const output = fs.createWriteStream(logfile);
      // proc.stdout.pipe(output);
      // proc.stderr.pipe(output);

      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Wine command failed with code ${code}`);
          res(`Wine command with code ${code}`);
        } else {
          res("");
        }
      });
      // wineCmd.run().then(res, rej);
    });
  }

  public openFolder(
    target?: string,
    bottle?: string,
    openType?: Array<
      | "openFile"
      | "openDirectory"
      | "multiSelections"
      | "showHiddenFiles"
      | "createDirectory"
      | "promptToCreate"
      | "noResolveAliases"
      | "treatPackageAsDirectory"
      | "dontAddToRecent"
    >
  ): Promise<string> {
    return new Promise<any>(async (res, _rej) => {
      let defaultPath = WorkDirConfig.HomeDir;
      if (target === "icon") {
        defaultPath = WorkDirConfig.IconsDir;
      } else if (target === "bottle") {
        if (bottle === undefined) {
          bottle = WorkDirConfig.ContainerName;
        }
        defaultPath = path.join(WorkDirConfig.ContainerDir, bottle, "drive_c");
      }
      if (openType === undefined) {
        openType = ["openFile"];
      }

      const files = await dialog.showOpenDialog({
        defaultPath: defaultPath,
        properties: openType,
      });
      if (!files.canceled && files.filePaths.length > 0) {
        const filePath = files.filePaths[0];
        // 处理选择的本地文件路径
        logger.info("<ct-test>:选择的文件为:", filePath);
        res(filePath);
      } else {
        res("");
      }
    });
  }

  public openFolderChooseFile(): Promise<any> {
    return new Promise<any>(async (res, _rej) => {
      dialog
        .showSaveDialog({
          // defaultPath: WorkDirConfig.HomeDir,
          properties: ["createDirectory"],
        })
        .then((files) => {
          if (!files.canceled && files.filePath) {
            let filePath = files.filePath;
            logger.info("<ct-test>:选择的文件为:", filePath);
            res(filePath);
          } else {
            res("");
          }
        });
    });
  }
  public fileType(path): Promise<any> {
    return new Promise<any>((res, _rej) => {
      try {
        const stats = fs.statSync(path);
        if (stats.isFile()) {
          logger.info(`${path} is a file.`);
          res("file");
        } else if (stats.isDirectory()) {
          logger.info(`${path} is a directory.`);
          res("directory");
        }
      } catch (err) {
        res("");
      }
    });
  }

  //从配置文件default.json 读取第一个软件的名字，启动参数，环境变量，ExecFilePath和Icon
  public getFirstSoftwareInfo(): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let softInfo = StatusManager.getInstance().getInstalledSoftInfo().apps;
      let info = {
        Name: "",
        Exec: "",
        Icon: "",
        Env: {},
        Param: "",
      };
      if (Object.keys(softInfo).length === 0) {
        res(info);
        return;
      } else {
        let key = Object.keys(softInfo)[0];
        info.Name = softInfo[key]["conf"]["Name"] || key;
        info.Exec = softInfo[key]["local"].ExecFilePath || "";
        info.Param = softInfo[key]["conf"].Executable.arguments;
        info.Env = {
          ...info.Env,
          ...softInfo[key]["conf"].Executable.wineenv,
        };
        let param = {
          icon: softInfo[key]["local"].Icon,
          exec: info.Exec,
          key: key,
        };
        this.iconPathCompletion(param).then((iconPath) => {
          info.Icon = iconPath || "";
          res(info);
        });
      }
    });
  }

  public iconPathCompletion(data): Promise<any> {
    return new Promise<any>((res, rej) => {
      let dir = path.join(WorkDirConfig.IconsDir);
      let found = false;
      let files = fs.readdirSync(dir);
      files.forEach((file) => {
        if (file.startsWith(data.icon)) {
          res(path.join(dir, file));
          found = true;
          return;
        }
      });
      //没有找到图标，先选择根据url下载图片，没有的话，根据md5生成图标
      if (!found) {
        let detailInfo =
          StatusManager.getInstance().getInstalledSoftInfo().apps;
        let iconUrl = detailInfo[data.key]["index"].Icon;
        if (iconUrl !== undefined && iconUrl !== "" && iconUrl !== null) {
          ExportIcon.downloadIcon(iconUrl, data.key, data.exec).then((icon) => {
            res(icon);
            return;
          });
        } else {
          ExportIcon.getIcon(data.exec, data.icon).then(
            (iconPath) => {
              res(iconPath);
            },
            (reason) => {
              logger.error("Get icon fail:" + reason);
              res("");
            }
          );
        }
      } else {
        res("");
      }
    });
  }

  private runCmd(cmd): Promise<any> {
    return new Promise<any>((res, _rej) => {
      logger.debug(`Running command: ${cmd}`);

      let stdios = [null, null, null] as any[];
      let resultString: string = "";
      const proc = spawn(cmd, {
        shell: true,
        stdio: stdios,
      });
      proc.stdout.on("data", (data) => {
        logger.debug(`${iconv.decode(data, "gbk").replaceAll("\n", "")}`);
      });

      proc.stderr.on("data", (data) => {
        logger.error(`${iconv.decode(data, "gbk").replaceAll("\n", "")}`);
        resultString = resultString + iconv.decode(data, "gbk");
      });
      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Command failed with code ${code}`);
          logger.debug(resultString);
          res(resultString);
          return;
        } else {
          res("");
        }
      });
    });
  }

  public getWinFixDll(): Promise<any> {
    return new Promise<any>((res, rej) => {
      let cmd = "python3 " + this._buildScript + " dll -l";
      this.runCmd(cmd).then((cmdRes) => {
        if (cmdRes === "") {
          if (!fileExists(this._dllList)) {
            rej("获取dll列表失败");
            return;
          }
          let dllList = JSON.parse(loadConfig(this._dllList));
          let dlls: string[] = [];
          for (let item in dllList) {
            if (item === "WinDllFix.exe") {
              continue;
            }
            for (let i in dllList[item]) {
              dlls.push(i + "." + item);
            }
          }
          logger.debug(dllList);
          if (!dllList) {
            rej("获取dll列表为空");
            return;
          }
          res(dlls);
        } else {
          rej("运行打包脚本失败");
          return;
        }
      });
    });
  }

  public setWinFixDll(dlls: string[], type: string): Promise<any> {
    return new Promise<any>((res, rej) => {
      let cmd = "python3 " + this._buildScript + " dll -d ";
      for (let item of dlls) {
        cmd += '"' + item + '" ';
      }
      cmd += `-t ${type}`;
      this.runCmd(cmd).then((cmdRes) => {
        if (cmdRes === "") {
          res("运行打包脚本成功");
        } else {
          rej("运行打包脚本失败");
          return;
        }
      });
    });
  }
  // 获取容器管理界面的容器信息，包括windows版本，依赖列表，软件列表 (根据Syswow64判断当前容器是32位还是64位)
  public getContainerInfo(): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let containerInfo = {
        Wine: "",
        Windows: "",
        Software: [] as {}[],
        Dependency: [] as string[],
      };
      let Syswow64Path = path.join(
        WorkDirConfig.ContainerDir,
        WorkDirConfig.ContainerName,
        "drive_c",
        "windows",
        "syswow64"
      );
      let promise1 = this.getWindowsVersion();
      let promise2 = this.getSoftwareAndDependencyList();
      let promise3 = this.getWineVersion();
      Promise.all([promise1, promise2, promise3])
        .then(([win, list, wine]) => {
          containerInfo.Software = list.Soft;
          containerInfo.Dependency = list.Deps;
          containerInfo.Wine = wine;
          if (win === "") {
            containerInfo.Windows = "";
          } else {
            if (fileExists(Syswow64Path)) {
              containerInfo.Windows = win
                .replace(/^Microsoft\s+/, "")
                .concat("（64位）");
            } else {
              containerInfo.Windows = win
                .replace(/^Microsoft\s+/, "")
                .concat("（32位）");
            }
          }
          res(containerInfo);
        })
        .catch((err) => {
          res(containerInfo);
          logger.error(err);
        });
    });
  }
  public getWindowsVersion(): Promise<string> {
    return new Promise((res, _rej) => {
      const readline = require("readline");
      let containerPath = path.join(
        WorkDirConfig.ContainerDir,
        WorkDirConfig.ContainerName,
        "system.reg"
      );
      if (!fileExists(containerPath)) {
        logger.error("注册表未找到");
        res("");
        return;
      }
      let fileStream = fs.createReadStream(containerPath);
      let rl = readline.createInterface({
        input: fileStream,
        crlfDelay: Infinity,
      });
      let flag1 = false; //[Software\\Microsoft\\Windows NT\\CurrentVersion]
      let flag2 = false; //"ProductName"
      rl.on("line", (line) => {
        if (!flag1) {
          if (
            line.includes(
              "[Software\\\\Microsoft\\\\Windows NT\\\\CurrentVersion]"
            )
          ) {
            flag1 = true;
          }
        } else {
          if (line.includes('"ProductName"=')) {
            res(line.split('"ProductName"="')[1].split('"')[0]);
            flag2 = true;
            rl.close();
            return;
          }
        }
      });
      rl.on("close", () => {
        if (!flag1) {
          logger.error(
            "未找到[Software\\Microsoft\\Windows NT\\CurrentVersion] "
          );
          res("");
        } else if (!flag2) {
          logger.error("未找到字段ProductName");
          res("");
        }
      });
      rl.on("error", (error) => {
        logger.error("监听错误: Error reading file:", error);
        res("");
      });
    });
  }
  public getSoftwareAndDependencyList(): Promise<any> {
    return new Promise((res, _rej) => {
      let list = {
        Soft: [] as {}[],
        Deps: [] as {}[],
      };
      let detailInfo = StatusManager.getInstance().getInstalledSoftInfo();
      Object.entries(detailInfo.apps).forEach((info) => {
        let key = info[0];
        let param = {
          icon: detailInfo.apps[key].local.Icon,
          exec: detailInfo.apps[key].local.ExecFilePath,
          key: key,
        };
        this.iconPathCompletion(param).then((iconPath) => {
          list.Soft.push({
            Name: detailInfo.apps[key]["conf"]["Name"],
            Icon: iconPath,
          });
        });
      });
      Object.entries(detailInfo.deps).forEach((info) => {
        let key = info[0];
        let version;
        if (
          detailInfo.deps[key].Version &&
          detailInfo.deps[key].Version !== "0"
        ) {
          version = "(" + detailInfo.deps[key].Version + ")";
        } else {
          version = "";
        }
        list.Deps.push({
          Name: key,
          Version: version,
          Description: detailInfo.deps[key]["Description"],
        });
      });
      res(list);
    });
  }
  public getWineVersion(): Promise<string> {
    return new Promise((res, _rej) => {
      try {
        let link = execSync(`readlink -f "${WorkDirConfig.Wine}"`)
          .toString()
          .trim();
        let wine = execSync(`dpkg -S "${link}"`)
          .toString()
          .trim()
          .split(":")[0];
        let stdout = execSync(`dpkg -l | grep "${wine}"`).toString();
        let lines = stdout.split("\n");
        for (let line of lines) {
          let col = line.trim().split(/\s+/);
          if (col.length >= 3 && col[1] === wine) {
            ContainerManager.wine.push(wine, col[2]);
            res(`${wine}（${col[2]}）`);
            return;
          }
        }
        res(`${wine}（未知版本）`);
        return;
      } catch (error) {
        res("");
        return;
      }
    });
  }

  public createShortcut(data): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      const wineCmd = new WineCommand(
        data["config"]["Exec"],
        data["config"]["Env"],
        data["config"]["Param"],
        data.bottle,
        true,
        false
      );
      const [errorMsg, cmdstr] = wineCmd.buildCommand();
      if (errorMsg !== "") {
        reject(errorMsg);
        return;
      } else {
        generateShortcut(
          data.bottle,
          data["config"]["Name"],
          cmdstr,
          data["config"]["Exec"],
          data["config"]["Folder"]
        ).then(
          (res) => {
            resolve(res);
          },
          (rej) => {
            reject(rej);
          }
        );
      }
    });
  }

  //导出配置文件-获取软件配置文件
  public getInstalledSoftInfo(name): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let detailInfo = StatusManager.getInstance().getInstalledSoftInfo();
      if (!name || !detailInfo.apps[name]) {
        res({
          baseInfo: {
            Arch: "",
            Name: "",
            Description: "",
            Grade: "Gold",
            Category: "",
            Dependencies: "",
            Icon: "",
            DesktopIcon: "",
            DetailUrl: "",
            Publish: true,
            LinuxArch: ["x86_64", "aarch64"],
          },
          installPack: [],
          startParam: {
            name: "",
            path: "",
            arguments: "",
            wineenv: "",
            run_in_window: false,
          },
        });
        return;
      }
      let config = detailInfo.apps[name]["conf"];
      let index = detailInfo.apps[name]["index"];
      let ret = {
        baseInfo: {
          Arch: config["Arch"],
          Name: config["Name"],
          Description: index["Description"],
          Grade: index["Grade"],
          Category: index["Category"],
          Dependencies: config["Dependencies"],
          Icon: index["Icon"],
          DesktopIcon: config["Icon"] ? config["Icon"] : "",
          DetailUrl: index["DetailUrl"],
          Publish: index["Publish"],
          LinuxArch: index["LinuxArch"],
        },
        installPack: config["Steps"].map((item) => {
          if (item.action === "install") {
            return {
              action: item.action,
              file_type: item.file_type,
              file_name: item.file_name,
              protable: item.protable,
              url: item.url,
            };
          } else if (item.action === "run_script") {
            return {
              action: item.action,
              script: item.script,
            };
          } else {
            return item;
          }
        }),
        startParam: {
          name: config["Executable"]["name"],
          path: config["Executable"]["path"],
          wineenv: config["Executable"]["wineenv"],
          arguments: config["Executable"]["arguments"],
          run_in_window: config["Executable"]["run_in_window"],
        },
      };
      res(ret);
    });
  }

  //导出配置文件-保存软件配置文件
  public exportSoftConfig(dir, param): Promise<any> {
    return new Promise<any>((res, _rej) => {
      let index = {};
      param.installPack.map((item) => {
        if (item.hasOwnProperty("protable")) {
          item.protable = item.protable === "true";
        }
      });

      index[param.baseInfo.Name] = {
        Arch: param.baseInfo.Arch,
        Name: param.baseInfo.Name,
        Description: param.baseInfo.Description,
        Grade: param.baseInfo.Grade,
        Icon: param.baseInfo.Icon,
        LinuxArch: param.baseInfo.LinuxArch,
        Publish: param.baseInfo.Publish,
        Category: param.baseInfo.Category,
        DetailUrl: param.baseInfo.DetailUrl,
      };

      let soft = {
        Name: param.baseInfo.Name,
        Description: param.baseInfo.Description,
        DetailUrl: param.baseInfo.DetailUrl,
        Dependencies: param.baseInfo.Dependencies,
        Grade: param.baseInfo.Grade,
        Arch: param.baseInfo.Arch,
        LinuxArch: param.baseInfo.LinuxArch,
        Executable: param.startParam,
        Steps: param.installPack,
      };
      if (param.baseInfo.DesktopIcon) {
        soft["Icon"] = param.baseInfo.DesktopIcon;
      }

      let exportDir = path.join(dir, param.baseInfo.Name);
      try {
        if (!fs.existsSync(exportDir)) {
          fs.mkdirSync(exportDir, { recursive: true });
        }
        saveConfig(path.join(exportDir, "index.yml"), yaml.stringify(index));
        saveConfig(
          path.join(exportDir, param.baseInfo.Name + ".yml"),
          yaml.stringify(soft)
        );
      } catch (error) {
        res(error);
      }

      res("");
    });
  }

  public ipcProcess(action: string, data: any): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "debuildEnvironment":
          this.debuildEnvironment().then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "getDebuildConfig":
          this.getDebuildConfig().then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "kylinWineDebuild":
          ContainerManager._containerStatus.kylinWineBuild = true;
          this.kylinWineDebuild(data).then(
            (res) => {
              ContainerManager._containerStatus.kylinWineBuild = false;
              resolve(res);
            },
            (rej) => {
              ContainerManager._containerStatus.kylinWineBuild = false;
              reject(rej);
            }
          );
          break;
        case "createBackup":
          ContainerManager._containerStatus.createBackup = true;
          this.createBackup(data).then(
            (res) => {
              ContainerManager._containerStatus.createBackup = false;
              resolve(res);
            },
            (rej) => {
              ContainerManager._containerStatus.createBackup = false;
              reject(rej);
            }
          );
          break;
        case "importBackup":
          ContainerManager._containerStatus.importBackup = true;
          this.importBackup(data).then(
            (res) => {
              ContainerManager._containerStatus.importBackup = false;
              resolve(res);
            },
            (rej) => {
              ContainerManager._containerStatus.importBackup = false;
              reject(rej);
            }
          );
          break;
        case "startExe":
          this.startExe(data).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "openFolder":
          this.openFolder(data.target, data.bottle, data.openType).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "openFolderChooseFile":
          this.openFolderChooseFile().then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "iconPathCompletion":
          this.iconPathCompletion(data).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "getContainerStatus":
          resolve(ContainerManager._containerStatus);
          break;
        case "fileType":
          this.fileType(data.path).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
          break;
        case "openDriverC":
          let openDriverC =
            "xdg-open " +
            path.join(WorkDirConfig.ContainerDir, data, "drive_c");
          this.runCmd(openDriverC).then();
          break;
        case "wineKillAll":
          this.runCmd(WorkDirConfig.WineKill).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "getWinFixDll":
          this.getWinFixDll().then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "setWinFixDll":
          this.setWinFixDll(data.dlls, data.type).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "getContainerInfo":
          this.getContainerInfo().then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "createShortcut":
          this.createShortcut(data).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "getInstalledSoftInfo":
          this.getInstalledSoftInfo(data).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        case "exportSoftConfig":
          this.exportSoftConfig(data.path, data.data).then(
            (res) => {
              resolve(res);
            },
            (rej) => {
              reject(rej);
            }
          );
          break;
        default:
          reject("Unsupport action");
      }
    });
  }

  public distory(): void {}
}
