/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import {
  Grade,
  GradeEnum,
  InstallStatus,
  LinuxArchEnum,
  ProgramCategory,
  ProgramInfo,
  ProgramRun,
  WinArchEnum,
  depInfo,
  depInfoLicense,
  DepStatus,
} from "../commons/define";
import { BaseManager } from "./Base";
import { Step } from "../commons/define";
import * as path from "path";
import { ipcMain, app } from "electron";

import {
  ProgramInstallStep,
  ProgramRunScriptStep,
} from "../internal/ProgramSetps";
import { ProgramExec } from "../internal/ProgramExec";
import { RelationManager } from "./RelationManager";
import { logger } from "../utils/Logger";
import { BrowserWindow, shell, net, dialog, globalShortcut } from "electron";
import contextMenu from "electron-context-menu";

import { WorkDirConfig } from "../commons/config";
import { loadConfig, saveConfig, title } from "../utils/Utils";
import { StatusManager } from "./StatusManager";
import { ContainerManager } from "./ContainerManager";
import { execSync } from "child_process";
export class ProgramManager extends BaseManager {
  protected _repo: string = WorkDirConfig.AppWarehouse;
  private _programs: ProgramInfo[] = [] as ProgramInfo[];
  private static instance: ProgramManager;
  private constructor() {
    super();
  }
  /**
   * 获取ProgramManager实例
   * @returns ProgramManager
   * @example
   * ```ts
   * ProgramManager.getInstance()
   * ```
   */
  public static getInstance(): ProgramManager {
    if (!this.instance) {
      this.instance = new ProgramManager();
    }
    return this.instance;
  }

  /**
   * 获取应用运行信息
   *
   * @param category 应用分类
   * @param name 应用名称
   * @returns Promise<ProgramRun>
   * @example
   * ```ts
   * ProgramManager.getInstance().getProgramRun('wechat').then(result=>{
   *   // do something
   * }, err=>{
   *   // do error
   * })
   * ```
   */
  public getProgramRun(category: ProgramCategory, key: string): Promise<any> {
    return new Promise<any>((res, rej) => {
      if (key == "") {
        res(undefined);
        return;
      }
      //let configPath = `${category}/${key}`
      let filepath = `${category}/${key}.yml`;
      this.loadConfig(filepath).then(
        (data) => {
          let steps: Step[] = [] as Step[];
          let programRun: ProgramRun = Object.assign(new ProgramRun(), data);
          programRun.Executable = Object.assign(
            new ProgramExec(),
            data["Executable"]
          );
          data["Steps"].forEach((s, _i) => {
            switch (s.action) {
              case "install":
                steps.push(Object.assign(new ProgramInstallStep(), s));
                break;
              case "run_script":
                steps.push(Object.assign(new ProgramRunScriptStep(), s));
                break;
            }
          });
          programRun.Key = key;
          programRun.Steps = steps;
          res(programRun);
        },
        (err) => {
          logger.error("on program run", err);
          rej(err);
        }
      );
    });
  }

  /**
   * 获取应用列表
   * @param pageno 分页页码，页码从1开始计数
   * @param pagesize 分页大小
   * @returns Promise<ProgramInfo[]>
   * @example
   * ```ts
   * ProgramManager.getInstance().getPrograms(1,10).then(result=>{
   *   // do something
   * }, err=>{
   *   // do error
   * })
   * ```
   */
  public getPrograms(): Promise<ProgramInfo[]> {
    return new Promise<ProgramInfo[]>((res, rej) => {
      if (this._programs.length == 0) {
        let programs: ProgramInfo[] = [] as ProgramInfo[];
        this.loadConfig("index.yml").then(
          (data) => {
            for (let k in data) {
              let program: ProgramInfo = Object.assign(
                new ProgramInfo(),
                data[k]
              );
              program.Key = k;

              if (!GradeEnum[program.Grade]) {
                logger.error("Grade invalid:", k, ",", program.Grade);
                continue;
              }
              let invalidArch = [] as string[];
              program.LinuxArch.forEach((arch) => {
                if (!LinuxArchEnum[arch]) {
                  invalidArch.push(arch);
                }
              });
              if (invalidArch.length > 0) {
                logger.error("LinuxArch invalid:", k, ",", invalidArch);
                continue;
              }
              if (!WinArchEnum[program.Arch]) {
                logger.error("WinArch invalid:", k, ",", program.Arch);
                continue;
              }

              if (
                program.Publish === false &&
                process.env.WINE_ASSISTANT_DEBUG != "true"
              ) {
                continue;
              }
              programs.push(program);
            }
            programs.sort((a, b) => {
              const codeFunc = (grade: Grade): number => {
                switch (grade) {
                  case GradeEnum.Gold:
                    return 4;
                  case GradeEnum.Silver:
                    return 3;
                  case GradeEnum.Bronze:
                    return 2;
                  case GradeEnum.Fail:
                    return -1;
                  default:
                    return 0;
                }
              };
              return codeFunc(b.Grade) - codeFunc(a.Grade);
            });
            this._programs = programs;
            res(this._programs);
          },
          (err) => {
            rej(err);
          }
        );
      } else {
        res(this._programs);
      }
    });
  }
  public ipcProcess(action: string, data: any): Promise<any> {
    //throw new Error("Method not implemented.");
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "openDetail":
          this.getProgramRun(data.Category, data.Key).then(
            (run: ProgramRun) => {
              let downUrl = "";
              for (let i = 0; i < run.Steps.length; i++) {
                if (run.Steps[i].action == "install") {
                  downUrl = (run.Steps[i] as ProgramInstallStep).url;
                  break;
                }
              }
              this.openDetail(run.DetailUrl, downUrl, run.Name).then(
                (url) => {
                  if (url != "") {
                  }
                  resolve(url);
                },
                (reason) => {
                  reject(reason);
                }
              );
            },
            (reason) => {
              reject(reason);
            }
          );
          break;
        case "getPrograms":
          this.getPrograms().then(
            (res) => {
              resolve(res);
            },
            (err) => {
              reject(err);
            }
          );
          break;
        case "getDependencies":
          this.getProgramRun(data.Category, data.Key).then(
            (run: ProgramRun) => {
              if (
                run == undefined ||
                run.Dependencies == null ||
                run.Dependencies.length == 0
              ) {
                resolve([]);
              } else {
                RelationManager.getInstance().recursiveDependencies(
                  data.Key,
                  run,
                  1,
                  (deps, err: string): void => {
                    if (err != "") {
                      logger.error(err);
                      reject("检查依赖关系失败");
                    } else {
                      //   logger.debug(deps);
                      resolve(deps);
                    }
                  },
                  (reason) => {
                    reject(reason);
                  }
                );
              }
            },
            (reason) => {
              reject(reason);
            }
          );
          break;
        case "getProgramDepsLicense":
          // resolve('ok');
          logger.debug("keming4321..... getProgramDepsLicense");
          logger.debug(data.Category);
          logger.debug(data.Key);
          this.getProgramRun(data.Category, data.Key).then(
            (run: ProgramRun) => {
              if (
                run == undefined ||
                run.Dependencies == null ||
                run.Dependencies.length == 0
              ) {
                resolve([]);
              } else {
                RelationManager.getInstance().recursiveDependencies(
                  data.Key,
                  run,
                  1,
                  (deps: depInfo[], err: string): void => {
                    if (err != "") {
                      logger.error(err);
                      reject("检查依赖关系失败");
                    } else {
                      logger.debug(deps);
                      this.getDepInfo(
                        deps,
                        data.Bottle || WorkDirConfig.ContainerName
                      ).then((result) => {
                        if (result.isAllInstall) {
                          logger.debug(JSON.stringify(result.deps));
                          resolve(JSON.stringify(result.deps));
                        } else {
                          this.openLicensePage(data.Key, result.deps).then(
                            (res) => {
                              logger.debug(
                                "keming4321..... openLicensePage return"
                              );
                              logger.debug(res);
                              resolve(res);
                            }
                          );
                        }
                      });
                    }
                  },
                  (reason) => {
                    reject(reason);
                  }
                );
              }
            },
            (reason) => {
              reject(reason);
            }
          );
          break;
        case "loadInstallConfig":
          this.loadInstallConfig().then(
            (res) => {
              resolve(res);
            },
            (err) => {
              reject(err);
            }
          );
          break;
        default:
          reject("Unsupport action");
      }
    });
  }

  public loadInstallConfig(): Promise<any> {
    return new Promise<any>((res, _rej) => {
      const allInfo = [] as {}[];
      let softInfo = StatusManager.getInstance().getInstalledSoftInfo().apps;
      let containerManager = ContainerManager.getInstance();
      let length = Object.keys(softInfo).length;
      Object.entries(softInfo).forEach((info) => {
        let key = info[0];
        let param = {
          icon: softInfo[key].local.Icon,
          exec: softInfo[key].local.ExecFilePath,
          key: key,
        };
        containerManager
          .iconPathCompletion(param)
          .then((iconPath) => {
            allInfo.push({
              name: softInfo[key].index.Name || "",
              info: softInfo[key].index.Description || "",
              key: key,
              category: softInfo[key].index.Category || "",
              date: softInfo[key].local.Date || "",
              icon: iconPath,
            });
          })
          .then(() => {
            if (length === allInfo.length) {
              res(allInfo);
            }
          });
      });
    });
  }

  public openLicensePage(key: string, deps: depInfoLicense[]): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      contextMenu({
        showSelectAll: false,
        showSaveLinkAs: true,
        showInspectElement: false,
      });

      let licenseWindow = new BrowserWindow({
        width: 600,
        height: 450,
        minWidth: 600,
        minHeight: 450,
        show: false,
        // frame: false,
        title: title()[1],
        autoHideMenuBar: true,
        webPreferences: {
          contextIsolation: true, // 是否开启隔离上下文
          nodeIntegration: true, // 渲染进程使用Node API
          sandbox: false,
          webviewTag: true, // 启用webview
          webSecurity: false, // 允许从本地文件系统中加载资源
          //nativeWindowOpen: false,
          preload: path.join(__dirname, "./licensePreloader.js"), // 需要引用js文件
        },
      });
      if (app.isPackaged) {
        licenseWindow.loadURL(
          `file://${path.join(__dirname, "./index.html")}#license`
        );
      } else {
        logger.debug("keming4321..... 创建licenseWindow");
        // 打开一个网址链接
        let url = "http://localhost:3000"; // 本地启动的vue项目路径
        licenseWindow.loadURL(url + "#" + "license");
        if (import.meta.env.DEV) {
          licenseWindow.webContents.openDevTools();
        }
      }

      let depItems: depInfo[] = [] as depInfo[];
      licenseWindow.show();
      licenseWindow.webContents.on("did-finish-load", () => {
        logger.debug("did-finish-load");
        //licenseWindow.webContents.send("licensePage", { licenseUrl: run.License_url, category: run.Category, key: run.Key });
        licenseWindow.webContents.send("licensePage", {
          deps: deps,
          key: key,
        });
      });
      ipcMain.on("licensePage", (event: Electron.IpcMainEvent, data: any) => {
        logger.debug("keming4321..... licenseUrl");
        logger.debug(data);
        if (data.licenceAccepted == true) {
          depItems = data.depItems;
          logger.debug("<ct test>:勾选项为:", data.depItems);
          logger.debug("licenceAccepted is true ");
          resolve(depItems);
          //licenseWindow.close()；会导致下次再开启许可页面时，弹窗报错，窗口已销毁
          try {
            if (licenseWindow) licenseWindow.hide();
          } catch (uncaughtException) {}
        } else {
          logger.debug(data);
          logger.debug("<ct-test>:取消安装：", data.depItems);
          //licenseWindow.close(); 会导致下次再开启许可页面时，弹窗报错，窗口已销毁
          try {
            if (licenseWindow) licenseWindow.hide();
          } catch (uncaughtException) {}
        }
      });
      licenseWindow.webContents.on("destroyed", () => {
        logger.debug("on destoryed ");
      });

      licenseWindow.webContents.setWindowOpenHandler((detail) => {
        licenseWindow.webContents.session.downloadURL(detail.url);
        return { action: "deny" };
      });

      licenseWindow.on("closed", () => {
        logger.debug("on closed");
      });

      // licenseWindow.webContents.session.removeAllListeners();
    });
  }

  private urlPopup(pop: string, name: string, url?) {
    let winInfo = {
      width: 400,
      height: 176,
    };
    if (pop === "isNormalDownload") {
      winInfo.width = 400;
      winInfo.height = 190;
    } else if (pop === "isDownload") {
      winInfo.width = 380;
      winInfo.height = 176;
    } else if (pop === "selectUrl") {
      winInfo.width = 500;
      winInfo.height = 450;
    }
    contextMenu({
      showSelectAll: false,
      showSaveLinkAs: true,
      showInspectElement: false,
    });
    let urlWindow = new BrowserWindow({
      width: winInfo.width,
      height: winInfo.height,
      minWidth: winInfo.width,
      minHeight: winInfo.height,
      show: false,
      title: title()[1],
      autoHideMenuBar: true,
      webPreferences: {
        contextIsolation: true, // 是否开启隔离上下文
        nodeIntegration: true, // 渲染进程使用Node API
        preload: path.join(__dirname, "./licensePreloader.js"),
      },
    });

    if (app.isPackaged) {
      urlWindow.loadURL(
        `file://${path.join(__dirname, "./index.html")}#webPopup`
      );
    } else {
      // 打开一个网址链接
      let url = "http://localhost:3000"; // 本地启动的vue项目路径
      urlWindow.loadURL(url + "#" + "webPopup");
      if (import.meta.env.DEV) {
        urlWindow.webContents.openDevTools();
      }
    }

    urlWindow.show();
    urlWindow.webContents.on("did-finish-load", () => {
      logger.debug("did-finish-load");
      urlWindow.webContents.send("webPopup", {
        type: pop,
        name: name,
        url: url,
      });
    });

    return urlWindow;
  }

  public openDetail(
    detail: string,
    downUrl: string,
    pName: string
  ): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      contextMenu({
        showSelectAll: false,
        showSaveLinkAs: true,
        showInspectElement: false,
      });
      let detailWin = new BrowserWindow({
        width: 1280,
        height: 720,
        minWidth: 1280,
        minHeight: 720,
        title: title()[1],
        show: false,
        autoHideMenuBar: true,
        webPreferences: {
          contextIsolation: true, // 是否开启隔离上下文
          nodeIntegration: true, // 渲染进程使用Node API
        },
      });
      detailWin.show();
      detailWin.loadURL(detail);
      detailWin.webContents.on("dom-ready", (ev) => {});
      detailWin.webContents.on("will-navigate", (ev, url) => {
        if (url == downUrl) {
          logger.debug("will navigate", url);
        } else {
          ev.preventDefault();
          let urlWindow = this.urlPopup("isNormalDownload", pName);
          ipcMain.on("webPopup", (event: Electron.IpcMainEvent, data: any) => {
            if (urlWindow.isDestroyed()) {
              return;
            }
            if (detailWin.isDestroyed()) {
              urlWindow.hide();
              return;
            }
            if (data.isNormalDownload == true) {
              urlWindow.hide();
              detailWin.webContents.loadURL(url);
            } else {
              urlWindow.hide();
            }
          });
        }
      });
      detailWin.webContents.on("did-navigate", (ev, url, code, text) => {
        if (code >= 400) {
          reject(`${code}: ${text}`);
        }
      });
      if (import.meta.env.DEV) {
        detailWin.webContents.openDevTools();
      }
      detailWin.webContents.setWindowOpenHandler((detail) => {
        detailWin.webContents.session.downloadURL(detail.url);
        return { action: "deny" };
      });
      detailWin.webContents.on("destroyed", () => {
        logger.debug("on destoryed ");
        resolve(appUrl);
      });
      detailWin.on("closed", () => {
        logger.debug("on closed");
      });
      let appUrl = "";
      detailWin.webContents.session.removeAllListeners();
      detailWin.webContents.session.on(
        "will-download",
        (event, item, contents) => {
          item.setSavePath("/tmp/" + item.getFilename());
          item.cancel();
          logger.debug(item.getURLChain());
          let isDownload = false;
          for (const element of item.getURLChain()) {
            logger.debug(element);
            if (element === downUrl) {
              isDownload = true;
              break;
            }
          }
          if (isDownload) {
            let urlWindow2 = this.urlPopup("isDownload", pName);
            ipcMain.on(
              "webPopup",
              (event: Electron.IpcMainEvent, data: any) => {
                if (urlWindow2.isDestroyed()) {
                  return;
                }
                if (data.isDownload == true) {
                  urlWindow2.hide();
                  appUrl = item.getURL();
                  if (!detailWin.isDestroyed()) {
                    detailWin.close();
                  }
                } else {
                  urlWindow2.hide();
                }
              }
            );
          } else {
            //判断是下载链接or许可页的链接
            let licenseUrl = [
              "https://www.microsoft.com/web/webpi/eula/net_library_eula_enu.htm",
              "https://github.com/bottlesdevs/dependencies",
              "https://www.gnu.org/licenses/old-licenses/gpl-2.0.html",
            ];
            const isLicenseUrl = licenseUrl.some(
              (url) => url === item.getURL()
            );

            if (isLicenseUrl) {
              const win = new BrowserWindow({ width: 800, height: 600 });
              win.loadURL(item.getURL());
            } else {
              let urlWindow3 = this.urlPopup("selectUrl", pName, [
                item.getURL(),
                downUrl,
              ]);
              ipcMain.on(
                "webPopup",
                (event: Electron.IpcMainEvent, data: any) => {
                  if (urlWindow3.isDestroyed()) {
                    return;
                  }
                  if (detailWin.isDestroyed()) {
                    urlWindow3.hide();
                    return;
                  }
                  if (data.selectUrl === 0) {
                    urlWindow3.hide();
                    appUrl = item.getURL();
                    if (!detailWin.isDestroyed()) {
                      detailWin.close();
                      //detailWin.destroy();
                    }
                  } else if (data.selectUrl === 1) {
                    urlWindow3.hide();
                    appUrl = downUrl;
                    if (!detailWin.isDestroyed()) {
                      detailWin.close();
                      //detailWin.destroy();
                    }
                  } else {
                    urlWindow3.hide();
                    logger.debug("用户关闭了消息框");
                  }
                }
              );
            }
          }
        }
      );
    });
  }

  public getDepInfo(
    deps: depInfo[],
    bottle: string
  ): Promise<{ deps: depInfoLicense[]; isAllInstall: boolean }> {
    return new Promise((resolve, _reject) => {
      let isAllInstall: boolean = true;
      let installList: depInfoLicense[] = [];
      let depKey: string[] = [];
      let obj: {} = StatusManager.getInstance().getInstalledSoftInfo();
      let status;
      let isChecked: boolean;
      let disabled: boolean;
      for (let item of deps) {
        if (depKey.includes(item.Key)) {
          continue;
        } else {
          depKey.push(item.Key);
        }

        status = "";
        if (
          item.hasOwnProperty("Key") &&
          obj.hasOwnProperty("deps") &&
          obj["deps"].hasOwnProperty(item["Key"])
        ) {
          status = DepStatus.Installed;
          let command = `dpkg --compare-versions ${item["Version"]} gt ${
            obj["deps"][item["Key"]]["Version"] || "0"
          } 2>/dev/null; echo $?`;
          let temp = parseInt(
            execSync(command, { encoding: "utf8" }).trim(),
            10
          );
          if (temp === 0) {
            status = DepStatus.Update;
          }
        } else {
          status = DepStatus.Uninstall;
          isAllInstall = false;
        }
        if (status === DepStatus.Installed) {
          isChecked = true;
          disabled = true;
        } else {
          isChecked = false;
          disabled = false;
        }

        installList.push({
          Key: item.Key,
          Level: item.Level,
          licenseUrl: item.licenseUrl,
          HardDep: item.HardDep || [],
          Version: item.Version,
          Checked: isChecked,
          Disabled: disabled,
          Status: status,
        });
      } //for
      resolve({ deps: installList, isAllInstall: isAllInstall });
    });
  }

  public distory(): void {}
}
