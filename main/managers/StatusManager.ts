/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import chokidar from "chokidar";
import { WorkDirConfig } from "../commons/config";
import {
  UninstallInfo,
  InstallInfo,
  InstallStatus,
  IpcProcess,
  ProgramInfo,
  DependencyInfo,
} from "../commons/define";
import { removeDesktop, loadConfig, saveConfig } from "../utils/Utils";
import { ExportIcon } from "../utils/IconUtil";
import path from "path";
import fs from "fs";
import { IpcManager } from "./IpcManager";
import { logger } from "../utils/Logger";
import { Uninstaller } from "../utils/Uninstaller";
import { ProgramManager } from "./ProgramManager";
import { DependencyManager } from "./DependencyManager";

export class StatusManager extends IpcProcess {
  private static _instance: StatusManager | undefined = undefined;
  //  private _fileStatus: Map<string, FileStatus>;
  private _installStatus: { [key: string]: InstallInfo } = {};
  private _detailInfo: {
    apps: { [key: string]: any };
    deps: { [key: string]: any };
  } = { apps: {}, deps: {} };
  //private _downloadStatus: { [key: string]: DownloadFile } = {};
  private _addFileToKey: { [key: string]: string } = {};
  private _uninstalls: { [key: string]: string } = {};
  private _unlinkFileToKey: { [key: string]: string } = {};
  //  private _loaded: boolean = false;
  private _execWatcher: chokidar.FSWatcher;
  private _watchDirs: string[] = [
    path.join(
      WorkDirConfig.ContainerDir,
      WorkDirConfig.ContainerName,
      "drive_c"
    ),
  ];
  private _allPrograms: ProgramInfo[] = [] as ProgramInfo[];
  private _allDependencies: DependencyInfo[] = [] as DependencyInfo[];

  private constructor() {
    super();
    this.init();

    //setTimeout(this.doUninstallClean,2000)
    this.doUninstallClean();
    // 检测目录是否存在，不存在则创建
    this._watchDirs.forEach((dirPath: string) => {
      if (!fs.existsSync(dirPath)) {
        try {
          fs.mkdirSync(dirPath, { recursive: true });
        } catch (error: any) {
          logger.error(`创建目录失败：${dirPath}，错误信息：${error.message}`);
        }
      }
    });
    this._execWatcher = chokidar.watch(this._watchDirs, {
      followSymlinks: false,
      ignored: ["**/Temp/**/*"],
      depth: 10,
    });
    const newFileCallback = (file, stats) => {
      if (!stats?.isFile()) {
        return;
      }

      //获取文件名
      let filename = path.basename(file);
      let ext = path.extname(file);
      //检查是否为监听的文件
      let key = this._addFileToKey[filename];
      if (key !== undefined) {
        //是监听的文件，获取文件状态
        let fStatus = this._installStatus[key];
        if (fStatus !== undefined) {
          logger.debug(`watched file ${file}, key ${key}`);
          //获取到文件状态，更新ExecFilePath和InstallStatus
          let oldPath = fStatus.ExecFilePath;
          fStatus.ExecFilePath = file;
          this.updateInstallStatus(key, InstallStatus.Installed);
          this.updateDetailInfo(key, file, "");
          this._installStatus[key] = fStatus;
          if (oldPath && oldPath !== "") {
            delete this._unlinkFileToKey[oldPath];
          }
          //将全路径添加到删除文件监听列表中
          if (!file.substring(0, file.lastIndexOf("/")).endsWith("unpack")) {
            this._unlinkFileToKey[file] = key;
          }
        }
      } else if (ext.toLowerCase() == ".lnk") {
        //logger.debug("xwtest-----------------------", file);
        fs.unlinkSync(file);
      }
    };
    this._execWatcher.on("add", newFileCallback);
    this._execWatcher.on("unlink", (file) => {
      //获取文件名
      //let filename = path.parse(file).base;
      //检查是否为监听的文件
      let key = this._unlinkFileToKey[file];
      if (key !== undefined) {
        //是监听的文件，获取文件状态
        let fStatus = this._installStatus[key];
        if (fStatus !== undefined) {
          logger.debug(`unlinked file ${file}, key ${key}`);
          this.updateInstallStatus(key, InstallStatus.Uninstall);
          fStatus.ExecFilePath = undefined;
          this._installStatus[key] = fStatus;
          //delete this._unlinkFileToKey[file];
        }
      }
    });
  }
  private checkInstalStatus() {
    for (const key in this._installStatus) {
      const installStatus = this._installStatus[key];
      if (installStatus.ExecFilePath !== undefined) {
        if (
          installStatus.InstallStatus == InstallStatus.Installed &&
          !fs.existsSync(installStatus.ExecFilePath)
        ) {
          installStatus.InstallStatus = InstallStatus.Uninstall;
        } else if (
          installStatus.InstallStatus == InstallStatus.Uninstall &&
          fs.existsSync(installStatus.ExecFilePath)
        ) {
          installStatus.InstallStatus = InstallStatus.Installed;
        }
        if (installStatus.InstallStatus == InstallStatus.Installed) {
          this._unlinkFileToKey[installStatus.ExecFilePath] = key;
        }
        this._installStatus[key] = installStatus;
      }
    }
  }
  private init() {
    let data = loadConfig(WorkDirConfig.InstallStatusConfig);
    if (data != "") {
      let obj = JSON.parse(data);
      this._installStatus = Object.assign(this._installStatus, obj);
      this.checkInstalStatus();
    }
    this.initInstalledInfo();
  }

  private initInstalledInfo() {
    let defData = loadConfig(WorkDirConfig.ContainerInstallInfo);
    if (defData != "") {
      let obj2 = JSON.parse(defData);
      if (obj2.apps) {
        this._detailInfo.apps = obj2.apps;
      }
      if (obj2.deps) {
        this._detailInfo.deps = obj2.deps;
      }
    }
    let setAdd: Set<String> = new Set(); // 软件或依赖已安装，而default.json中没有记录
    let setUpdate: Set<String> = new Set(); //软件或依赖已安装，而default.json中格式需更新
    Object.entries(this._installStatus).forEach(([key, value]) => {
      if (value["InstallStatus"] === "Installed") {
        setAdd.add(key);
      }
    });
    Object.entries(this._detailInfo.apps).forEach((key) => {
      if (setAdd.has(key[0])) {
        setAdd.delete(key[0]);
        if (
          !this._detailInfo.apps[key[0]]["conf"] ||
          !this._detailInfo.apps[key[0]]["index"] ||
          !this._detailInfo.apps[key[0]]["local"]
        ) {
          setUpdate.add(key[0]);
        }
      }
    });
    Object.entries(this._detailInfo.deps).forEach((key) => {
      if (setAdd.has(key[0])) {
        setAdd.delete(key[0]);
      }
    });
    ProgramManager.getInstance()
      .getPrograms()
      .then((data) => {
        Object.assign(this._allPrograms, data);
        if (setAdd.size > 0 || setUpdate.size > 0) {
          let dataAdd = data.filter((k) => setAdd.has(k.Key));
          let dataUpdate = data.filter((k) => setUpdate.has(k.Key));
          if (dataAdd.length > 0) {
            dataAdd.map((ele) => {
              ProgramManager.getInstance()
                .getProgramRun(ele.Category, ele.Key)
                .then((result) => {
                  this._detailInfo.apps[ele.Key] = {
                    conf: result,
                    index: ele,
                    local: {
                      Date: this.getCurrentDate(),
                      ExecFilePath: this._installStatus[ele.Key].ExecFilePath,
                      Icon: ExportIcon.getMd5IconName(
                        this._installStatus[ele.Key].ExecFilePath || "",
                        ele.Key
                      ),
                    },
                  };
                });
            });
          } else if (dataUpdate.length > 0) {
            dataUpdate.map((ele) => {
              let Date = this._detailInfo.apps[ele.Key].Date;
              let ExecFilePath = this._detailInfo.apps[ele.Key].ExecFilePath;
              let Icon = this._detailInfo.apps[ele.Key].Icon;
              ProgramManager.getInstance()
                .getProgramRun(ele.Category, ele.Key)
                .then((result) => {
                  this._detailInfo["apps"][ele.Key] = {
                    conf: result,
                    index: ele,
                    local: {
                      Date: Date,
                      ExecFilePath: ExecFilePath,
                      Icon: Icon,
                    },
                  };
                });
            });
          }
        }
      });
    DependencyManager.getInstance()
      .getDependencies()
      .then((result) => {
        Object.assign(this._allDependencies, result);
        result = result.filter((k) => setAdd.has(k.Key));
        if (result.length > 0) {
          result.map((ele) => {
            if (!this._detailInfo.deps[ele.Key]) {
              this._detailInfo.deps[ele.Key] = {};
            }
            this._detailInfo["deps"][ele.Key].Description = ele.Description;
            this._detailInfo["deps"][ele.Key].Category = ele.Category;
            DependencyManager.getInstance()
              .getDependencyRun(ele.Category, ele.Key)
              .then((ver) => {
                this._detailInfo["deps"][ele.Key].Version = ver.Version;
              });
          });
        }
      });
  }

  public getInstalledSoftInfo() {
    return this._detailInfo;
  }

  public reloadInstallStatusConfig() {
    let data = loadConfig(WorkDirConfig.InstallStatusConfig);
    if (data != "") {
      let obj = JSON.parse(data);
      this._installStatus = Object.assign(this._installStatus, obj);
    }
  }

  public reloadInstallSoftInfo() {
    let data = loadConfig(WorkDirConfig.InstallStatusConfig);
    if (data != "") {
      let obj = JSON.parse(data);
      this._detailInfo = Object.assign(this._detailInfo, obj);
    }
  }

  public static getInstance() {
    if (!this._instance) {
      this._instance = new StatusManager();
    }
    return this._instance;
  }
  async ipcProcess(action: string, param: any): Promise<any> {
    //throw new Error("Method not implemented.");
    return new Promise<any>((resolve, reject) => {
      switch (action) {
        case "getInstallStatus":
          resolve(this.getInstallInfos(param as string[]));
          break;
        case "uninstallConfig":
          if (this._detailInfo.apps[param]) {
            delete this._detailInfo.apps[param];
          }
          break;
        default:
          reject("Unsupported action");
      }
    });
  }
  /*
  public updateIsProtable(key: string, isProtable: boolean) {
    if (this._installStatus[key] == undefined) {
      return false;
    }
    this._installStatus[key].IsProtable = isProtable;
  }
  */
  public updateUninstallInfo(
    key: string,
    uninstallKey: string,
    displayName: string,
    desktopName: string,
    isProtable: boolean = false
  ) {
    if (this._installStatus[key] == undefined) {
      return false;
    }
    var uninstaller: UninstallInfo = {
      UninstallKey: uninstallKey, //如果IsProtable为true，值为安装路径，否则为guid
      DesktopName: desktopName,
      DisplayName: displayName,
    };

    this._installStatus[key].UninstallInfo = uninstaller;
    this._installStatus[key].IsProtable = isProtable;
  }
  public addWatch(key: string, filename: string): boolean {
    logger.debug("on add watch", key, filename);
    if (this._installStatus[key] == undefined) {
      return false;
    }
    //this._fileStatus[key].WatchFile = filename;
    this._addFileToKey[filename] = key;
    return true;
  }

  public addUninstall(key: string, name: string) {
    if (this._installStatus[key] !== undefined) {
      this._uninstalls[key] = name;
    }
  }
  public removeInstallStatus(key: string) {
    let fStatus = this._installStatus[key];
    if (fStatus !== undefined) {
      delete this._installStatus[key];
      if (fStatus !== undefined) {
        delete this._addFileToKey[fStatus.ExecFilePath!];
        delete this._unlinkFileToKey[fStatus.ExecFilePath!];
      }
    }
  }

  public addDownloadFile(key: string, fileKey: string, fileName: string) {
    if (this._installStatus[key] !== undefined) {
      this._installStatus[key].DownloadFiles[fileKey] = fileName;
    }
  }
  public removeDownloadFile(key: string, fileKey: string) {
    if (this._installStatus[key] !== undefined) {
      delete this._installStatus[key].DownloadFiles[fileKey];
    }
  }

  /**
   * 更新文件状态
   * @param key 文件key
   * @param status 状态
   * @param progress? 下载进度, Undownload值为0，Downloaded值为100，其它情况值为(0,100)
   * @returns
   */
  public updateInstallStatus(
    key: string,
    status: InstallStatus,
    execPath?: string,
    //progress?: number
    version?: string
  ) {
    //let fStatus = this._fileStatus[key];
    if (this._installStatus[key] == undefined) {
      this._installStatus[key] = {
        //DownloadStatus: DownloadStatus.Undownload,
        InstallStatus: InstallStatus.Uninstall,
        DownloadFiles: {},
        //DownloadProgress: 0,
        ExecFilePath: undefined,
        IsProtable: false,
        UninstallInfo: { UninstallKey: "", DisplayName: "", DesktopName: "" },
      } as InstallInfo;
    }
    let oldStatus = this._installStatus[key].InstallStatus;
    if (oldStatus !== status) {
      IpcManager.sendToView("installStatus", {
        ChangedStatus: "InstallStatus",
        OldStatus: oldStatus,
        NewStatus: status,
        Key: key,
      });
      this._installStatus[key].InstallStatus = status;
      if (status === InstallStatus.Installed) {
        this.persistentStatus();
      }
    }
    if (status === InstallStatus.Installed && execPath !== undefined) {
      this._installStatus[key].ExecFilePath = execPath;
      this.updateDetailInfo(key, execPath, ""); //软件
    } else if (status === InstallStatus.Installed && version !== undefined) {
      this.updateDetailInfo(key, "", version); //依赖
    }
  }

  public updateDetailInfo(key: string, execPath: string, version: string) {
    if (execPath === "" && version === "") {
      return;
    }
    let soft = this._allPrograms.find((k) => k.Key === key);
    let dep = this._allDependencies.find((k) => k.Key === key);
    if (soft) {
      if (this._detailInfo.apps[key] == undefined) {
        ProgramManager.getInstance()
          .getProgramRun(soft.Category, soft.Key)
          .then((result) => {
            this._detailInfo.apps[key] = {
              conf: result,
              index: soft,
              local: {
                Date: this.getCurrentDate(),
                ExecFilePath: execPath,
                Icon: ExportIcon.getMd5IconName(execPath, key),
              },
            };
          })
          .then(() => {
            this.persistentStatus();
          });
      }
    } else if (dep) {
      this._detailInfo.deps[key] = {
        Description: dep.Description,
        Category: dep.Category,
        Version: version,
      };
    }
  }

  //处理正常逻辑卸载的应用，即uninstall键被删除的应用
  private doUninstallClean() {
    //logger.debug(this._uninstalls);
    if (Object.entries(this._uninstalls).length == 0) {
      setTimeout(() => {
        this.doUninstallClean();
      }, 2000);
      return;
    }

    let uninstaller = new Uninstaller();
    uninstaller.list().then((uninstalls) => {
      Object.entries(this._uninstalls).forEach((item) => {
        let fStatus = this._installStatus[item[0]];
        logger.debug(this._uninstalls);
        //logger.debug("xwtest---------------", uninstalls, item, fStatus);
        if (
          fStatus !== undefined &&
          fStatus.IsProtable === false &&
          uninstalls[fStatus.UninstallInfo.UninstallKey] === undefined
        ) {
          removeDesktop(fStatus.UninstallInfo.DesktopName);
          delete this._uninstalls[item[0]];
          //delete this._installStatus[item[0]];
          //this.removeFileStatus(item[0]);
        }
      });
      setTimeout(() => this.doUninstallClean(), 2000);
    });
  }
  //处理异常逻辑卸载的应用，即命令行删除的应用
  public isInstalled(key: string): boolean {
    let info = this.getInstallInfo(key);
    if (info && info.InstallStatus == InstallStatus.Installed) {
      return true;
    } else {
      return false;
    }
  }
  public getInstallInfo(key: string): InstallInfo | undefined {
    return this._installStatus[key];
  }

  public getInstallInfos(keys?: string[]): Object {
    if (keys !== undefined && keys.length > 0) {
      let r = {};
      Object.entries(this._installStatus).forEach((val) => {
        if (keys.indexOf(val[0]) >= 0) {
          r[val[0]] = val[1];
        }
      });
      return r;
    } else {
      return this._installStatus;
    }
  }
  private persistentStatus() {
    saveConfig(
      WorkDirConfig.InstallStatusConfig,
      JSON.stringify(this._installStatus, (key: string, value: any) => {
        if (key === "InstallStatus" && value === "Installing") {
          return "Uninstall";
        }
        return value;
      })
    );
    saveConfig(
      WorkDirConfig.ContainerInstallInfo,
      JSON.stringify(this._detailInfo)
    );
  }

  public getCurrentDate() {
    let currentDate = new Date();
    let year = currentDate.getFullYear();
    let month = String(currentDate.getMonth() + 1).padStart(2, "0");
    let day = String(currentDate.getDate()).padStart(2, "0");
    let currentDateString = `${year}-${month}-${day}`;
    return currentDateString;
  }

  distory() {
    this._execWatcher.close();
    this.persistentStatus();
  }
}
