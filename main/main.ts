/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
// electron-main/index.ts
import { app, BrowserWindow, ipcMain, Menu, screen, dialog } from "electron";
import path from "path";
import { ProgramManager } from "./managers/ProgramManager";
import { DownloaderManager } from "./managers/DownloaderManager";
import { DependencyManager } from "./managers/DependencyManager";
import { ProgramInternal } from "./internal/ProgramInternal";
import { DependencyInternal } from "./internal/DependencyInternal";
import { IpcManager } from "./managers/IpcManager";
import { StatusManager } from "./managers/StatusManager";
import { Wineboot } from "./utils/Wineboot";
import { SysInfoManager } from "./managers/SysInfoManager";
import { SoftInfoManager } from "./managers/SoftInfoManager";
import { ContainerManager } from "./managers/ContainerManager";
import { logger } from "./utils/Logger";
import { WorkDirConfig } from "./commons/config";
import { title } from "./utils/Utils";
import fs from "fs-extra";

let initflag = false;
function deleteHiddenDirectories() {
  const homeDirectory = process.env.HOME || process.env.USERPROFILE; // 获取当前用户的家目录
  if (!homeDirectory) {
    console.log("Unable to determine home directory");
    return;
  }
  const hiddenDirectories = [".okylin-wine", ".wine-assistant"]; // 要删除的隐藏目录名称列表

  hiddenDirectories.forEach((directory) => {
    const directoryPath = path.join(homeDirectory, directory); // 构建隐藏目录的完整路径
    logger.debug("<ct-test>:待删除目录:", directoryPath);

    // 检查目录是否存在
    if (fs.existsSync(directoryPath)) {
      // 检查路径是否指向目录
      if (fs.statSync(directoryPath).isDirectory()) {
        // 递归删除目录及其内容
        fs.rmdirSync(directoryPath, { recursive: true });
        console.log(`Deleted directory: ${directoryPath}`);
      } else {
        console.log(`Skipped: ${directoryPath} is not a directory`);
      }
    } else {
      console.log(`Skipped: ${directoryPath} does not exist`);
    }
  });

  function removeDesktop(dir, search) {
    fs.readdir(dir, (err, files) => {
      if (err) {
        logger.info("无法读取目录:", err);
        return;
      }

      // 过滤出包含特定字符串的文件
      files.forEach((file) => {
        const filePath = path.join(dir, file);

        const ext = path.extname(filePath);
        if (ext !== ".desktop") {
          return;
        }
        // 读取文件内容
        fs.readFile(filePath, "utf8", (err, data) => {
          if (err) {
            logger.info(filePath, err);
            return;
          }

          // 检查文件内容是否包含特定字符串
          if (data.includes(search)) {
            logger.info("Remove Desktop", filePath);
            // 删除文件
            fs.unlink(filePath, (err) => {
              if (err) {
                return console.log("无法删除文件: " + err);
              }
              console.log(`已删除文件: ${filePath}`);
            });
          }
        });
      });
    });
  }
  const localDesktop = path.join(
    homeDirectory,
    ".local",
    "share",
    "applications"
  );
  const searchString = path.join(homeDirectory, ".okylin-wine");
  removeDesktop(localDesktop, searchString);

  if (fs.existsSync(path.join(homeDirectory, "桌面"))) {
    removeDesktop(path.join(homeDirectory, "桌面"), searchString);
  } else if (fs.existsSync(path.join(homeDirectory, "Desktop"))) {
    removeDesktop(path.join(homeDirectory, "Desktop"), searchString);
  }

  // 读取目录内容
}
function checkMaxUserInstances(): void {
  const { exec } = require("child_process");
  exec("sysctl fs.inotify.max_user_instances", (error, stdout) => {
    if (error) {
      console.error(error);
      return;
    }
    const currentValue = parseInt(stdout.trim().split("=")[1]);

    if (currentValue < 1024) {
      let urlWindow = urlPopup("maxUserInstances");
      ipcMain.on("webPopup", (event: Electron.IpcMainEvent, data: any) => {
        if (urlWindow.isDestroyed()) {
          return;
        }
        if (win.isDestroyed()) {
          urlWindow.hide();
          return;
        }
        if (data.maxUserInstances) {
          urlWindow.hide();
        }
      });
    }
  });
}

const createWindow = (): BrowserWindow => {
  const win = new BrowserWindow({
    show: false,

    width: 1080,
    height: 720,
    minWidth: 1010,
    minHeight: 650,
    //frame:false,
    title: title()[1],
    webPreferences: {
      contextIsolation: true, // 是否开启隔离上下文
      nodeIntegration: true, // 渲染进程使用Node API
      preload: path.join(__dirname, "./preload.js"), // 需要引用js文件
    },
  });

  // 如果打包了，渲染index.html
  if (app.isPackaged) {
    win.loadFile(path.join(__dirname, "./index.html"));
  } else {
    let url = "http://localhost:3000/index.html"; // 本地启动的vue项目路径
    win.loadURL(url).then(() => {});
  }

  // win.maximize();
  win.show();
  return win;
};

function urlPopup(pop: string, content?: string) {
  console.log(pop);
  let winInfo = {
    width: 400,
    height: 176,
  };
  if (pop === "isReset" || pop === "cannotClose") {
    winInfo.width = 400;
    winInfo.height = 176;
  } else if (pop === "maxUserInstances") {
    winInfo.width = 450;
    winInfo.height = 200;
  } else if (pop === "About") {
    winInfo.width = 400;
    winInfo.height = 220; // 可以根据实际需求调整“关于”窗口的大小
  }

  let urlWindow = new BrowserWindow({
    width: winInfo.width,
    height: winInfo.height,
    show: false,
    title: title()[1],
    autoHideMenuBar: true,
    webPreferences: {
      contextIsolation: true, // 是否开启隔离上下文
      nodeIntegration: true, // 渲染进程使用Node API
      preload: path.join(__dirname, "./licensePreloader.js"),
    },
  });

  if (app.isPackaged) {
    urlWindow.loadURL(
      `file://${path.join(__dirname, "./index.html")}#webPopup`
    );
  } else {
    // 打开一个网址链接
    let url = "http://localhost:3000"; // 本地启动的vue项目路径
    urlWindow.loadURL(url + "#" + "webPopup");
    if (import.meta.env.DEV) {
      urlWindow.webContents.openDevTools();
    }
  }

  urlWindow.show();
  urlWindow.webContents.on("did-finish-load", () => {
    logger.debug("did-finish-load");
    urlWindow.webContents.send("webPopup", {
      type: pop,
      content: content || "",
    });
  });

  return urlWindow;
}

// //恢复出厂设置
// function envInit() {
//   const initoptions = {
//     type: "info",
//     title: "提示",
//     message: "恢复出厂设置，将卸载删除所有已安装软件包,并将重启wine助手!",
//     buttons: ["确认", "取消"],
//     cancelId: 2,
//   };
//   dialog.showMessageBox(win, initoptions).then((response) => {
//     if (response.response === 0) {
//       logger.debug("<ct-test>:确认恢复出厂设置");
//       initflag = true;
//       deleteHiddenDirectories();
//       app.relaunch();
//       app.exit();
//     } else if (response.response === 1) {
//       logger.debug("<ct-test>:取消恢复出厂设置");
//     } else {
//       logger.debug("<ct-test>:关闭恢复出厂设置对话框");
//     }
//   });
// }

function getWineAssistantInfo(): Promise<string> {
  return new Promise((resolve, reject) => {
    try {
      const childProcess = require("child_process");
      childProcess.exec(
        "dpkg -l | grep wine-assistant",
        (error, stdout, stderr) => {
          if (error) {
            console.error("获取wine助手信息失败：", error);
            resolve(""); // 如果出现错误，返回空字符串
            return;
          }
          const lines = stdout.split("\n");
          let name = "";
          let version = "";
          for (let line of lines) {
            if (line.trim() === "") continue;
            const cols = line.split(/\s+/);
            name = title()[1];
            version = cols[2];
            break; // 只取第一组匹配的数据，根据实际需求可调整是否取全部
          }
          const infoString = `${name}\n${version}`;
          resolve(infoString);
        }
      );
    } catch (innerError) {
      console.error("在获取wine助手信息过程中出现其他错误：", innerError);
      resolve("");
    }
  });
}

const devMenu: Electron.MenuItemConstructorOptions[] = [
  {
    label: "设置",
    submenu: [
      {
        label: "恢复出厂设置",
        click: () => {
          const urlWindow = urlPopup("isReset");
          ipcMain.on("webPopup", (event: Electron.IpcMainEvent, data: any) => {
            if (urlWindow.isDestroyed()) {
              return;
            }
            if (win.isDestroyed()) {
              urlWindow.hide();
              return;
            }
            if (data.isReset == true) {
              urlWindow.hide();
              logger.debug("<ct-test>:确认恢复出厂设置");
              initflag = true;
              deleteHiddenDirectories();
              app.relaunch();
              app.exit();
            } else {
              urlWindow.hide();
              logger.debug("<ct-test>:取消恢复出厂设置");
            }
          });
          // envInit();
        },
      },
    ],
  },
  {
    label: "帮助",
    click: () => {
      let apiWin = new BrowserWindow({
        width: 500,
        height: 500,
        title: title()[1],
        show: false,
      });
      apiWin.maximize();
      apiWin.show();
      apiWin.loadURL(
        "https://gitee.com/openkylin/win-program/wikis/wine%E5%8A%A9%E6%89%8B%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97?sort_id=8517296"
      );
      apiWin.on("closed", () => {});
    },
  },
  {
    label: "关于",
    click: () => {
      let urlWindow;
      getWineAssistantInfo()
        .then((wineInfos) => {
          // 直接将获取到的 wineInfos 传递给 urlPopup 函数进行处理
          urlWindow = urlPopup("About", wineInfos);
        })
        .catch((error) => {
          const errorString = `error\n${error.message}`;
          // 将格式化后的错误信息字符串传递给 urlPopup 函数进行处理
          urlWindow = urlPopup("About", errorString);
        });
      ipcMain.on("webPopup", (event: Electron.IpcMainEvent, data: any) => {
        if (urlWindow.isDestroyed()) {
          return;
        }
        if (win.isDestroyed()) {
          urlWindow.hide();
          return;
        }
      });
    },
  },
];
let win;
let EndFlag = {
  installSoft: true,
  installDepend: true,
  importBackup: false,
  createBackup: false,
};
let urlWindow;
let urlWindowShowing = false;
const gotTheLock = app.requestSingleInstanceLock();
if (!gotTheLock) {
  app.quit();
} else {
  app.on("second-instance", (event, commandLine, workingDirectory) => {
    // 当运行第二个实例时,将会聚焦到myWindow这个窗口
    if (win) {
      if (win.isMinimized()) win.restore();
      win.focus();
    }
  });
  if (app.isPackaged) {
    app.enableSandbox();
  }

  app.whenReady().then(() => {
    //console.log("xwtest-------------", screen.getPrimaryDisplay().workArea);
    win = createWindow(); // 创建窗口
    const iconPath = path.resolve(__dirname, "./wine-assistant.png");
    win.setIcon(iconPath);
    if (import.meta.env.DEV) {
      const m = Menu.buildFromTemplate(devMenu);
      Menu.setApplicationMenu(m);
      win.webContents.openDevTools();
    } else {
      const m = Menu.buildFromTemplate(devMenu);
      Menu.setApplicationMenu(m);
    }

    let ipcManager = IpcManager.getInstance();
    ipcManager.regProcess("program", ProgramManager.getInstance());
    ipcManager.regProcess("dependency", DependencyManager.getInstance());
    ipcManager.regProcess("programInternal", new ProgramInternal());
    ipcManager.regProcess("downloaderManager", DownloaderManager.getInstance());
    ipcManager.regProcess("statManager", StatusManager.getInstance());
    ipcManager.regProcess("dependencyInternal", new DependencyInternal());
    ipcManager.regProcess("sysInfo", new SysInfoManager());
    ipcManager.regProcess("container", ContainerManager.getInstance());
    ipcManager.regProcess("softInfo", new SoftInfoManager());
    ipcManager.startup();

    app.on("activate", () => {
      if (BrowserWindow.getAllWindows().length === 0) createWindow();
    });
    //initWineAssistant();
    initDebuildScritp();
    Wineboot.checkAndInitContainer();
    checkMaxUserInstances();
  });

  function initDebuildScritp() {
    if (app.isPackaged) {
      let buildPy = "/opt/wine-assistant/kylin-wine-bottle";
      if (WorkDirConfig.BuildVersion === "Commercial") {
        buildPy = "/opt/kylin-wine-assistant/kylin-wine-bottle";
      }
      if (fs.existsSync(buildPy)) {
        if (fs.existsSync(WorkDirConfig.DebuildDir)) {
          try {
            fs.unlinkSync(WorkDirConfig.DebuildDir);
          } catch (error) {
            fs.rmdirSync(WorkDirConfig.DebuildDir);
          }
        }
        fs.symlinkSync(buildPy, WorkDirConfig.DebuildDir, "dir");
      }
    }
  }

  process.on("uncaughtException", (error) => {
    logger.warn("uncaughtException: ", error);
  });

  ipcMain.on("restart-app", () => {
    initflag = false;
    app.relaunch();
    app.exit();
  });

  const options = {
    type: "info",
    title: "提示",
    message: "软件下载安装中,请勿关闭wine助手!!!",
    buttons: ["OK"],
  };

  ipcMain.on("installend", (event: Electron.IpcMainEvent, data: any) => {
    if (data.installSoftEndflag == true) {
      EndFlag.installSoft = true;
    }
    if (data.installSoftEndflag == false) {
      EndFlag.installSoft = false;
    }
    if (data.installDependEndFlag == true) {
      EndFlag.installDepend = true;
    }
    if (data.installDependEndFlag == false) {
      EndFlag.installDepend = false;
    }
  });
  ipcMain.on("backup", (event: Electron.IpcMainEvent, data: any) => {
    if (data.importBackupEndFlag == true) {
      EndFlag.importBackup = true;
    }
    if (data.importBackupEndFlag == false) {
      EndFlag.importBackup = false;
    }
    if (data.createBackupEndFlag == true) {
      EndFlag.createBackup = true;
    }
    if (data.createBackupEndFlag == false) {
      EndFlag.createBackup = false;
    }
  });
  // 关闭窗口
  app.on("will-quit", (event) => {
    if (process.platform !== "darwin") {
      app.quit();
    }
    IpcManager.getInstance().shutdown();
    process.exit(0);
  });
  // 在主程序退出之前执行的逻辑
  //关闭wine助手前，检查是否有依赖或软件正在安装，安装过程中阻止关闭wine助手，并弹窗提示
  ipcMain.on("closeNotice", (event: Electron.IpcMainEvent, data: any) => {
    if (
      EndFlag.installDepend &&
      EndFlag.installSoft &&
      !EndFlag.importBackup &&
      !EndFlag.createBackup
    ) {
      app.quit();
      IpcManager.getInstance().shutdown();
      if (initflag) {
        deleteHiddenDirectories();
        initflag = false;
      }
    } else {
      if (!EndFlag.installDepend && !urlWindowShowing) {
        urlWindow = urlPopup("cannotClose", "Depend");
        urlWindowShowing = true;
      } else if (!EndFlag.installSoft && !urlWindowShowing) {
        urlWindow = urlPopup("cannotClose", "Soft");
        urlWindowShowing = true;
      } else if (EndFlag.importBackup && !urlWindowShowing) {
        urlWindow = urlPopup("cannotClose", "importBackup");
        urlWindowShowing = true;
      } else if (EndFlag.createBackup && !urlWindowShowing) {
        urlWindow = urlPopup("cannotClose", "createBackup");
        urlWindowShowing = true;
      }
      ipcMain.on("webPopup", (event: Electron.IpcMainEvent, data: any) => {
        urlWindowShowing = false;
        if (urlWindow.isDestroyed()) {
          return;
        }
        if (win.isDestroyed()) {
          return;
        }
        if (data.confirm) {
          IpcManager.getInstance().shutdown();
          app.exit(0);
        } else {
          urlWindow.hide();
        }
      });
    }
  });
}
