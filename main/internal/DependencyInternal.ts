/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { DependencyInfo, InstallStatus, DepStatus } from "../commons/define";
import { DependencyManager } from "../managers/DependencyManager";
import { StatusManager } from "../managers/StatusManager";
import { BaseInternal } from "./Base";

export class DependencyInternal extends BaseInternal {
  private _dependencyManager: DependencyManager =
    DependencyManager.getInstance();
  private _statusManager = StatusManager.getInstance();
  ipcProcess(action: string, param: any): Promise<any> {
    return new Promise<any>((res, _rej) => {
      switch (action) {
        case "install":
          let depKeys = param as {
            Key: string;
            Level: number;
            Status: DepStatus;
          }[];
          if (depKeys.length == 0) {
            res([]);
            return;
          }

          let levelMap = new Map<number, string[]>();
          let levels = [] as number[];
          depKeys.forEach((val) => {
            if (!levelMap.has(val.Level)) {
              levels.push(val.Level);
              levelMap.set(val.Level, []);
            }
            levelMap.get(val.Level)?.push(val.Key);
            if (val.Status === DepStatus.Update) {
              this._statusManager.updateInstallStatus(
                val.Key,
                InstallStatus.Uninstall
              );
            }
          });
          let results = [] as object[];
          const installFunc = (level: number) => {
            if (level == undefined) {
              res(results);
              return;
            }
            let keys = levelMap.get(level) || [];
            this._dependencyManager
              .getDependencies(keys)
              .then((dInfos: DependencyInfo[]) => {
                //console.log(keys.length, dInfos.length);
                let ps = [] as Promise<{}>[];
                let errs = [] as {}[];
                dInfos.forEach((info) => {
                  let idx = keys.indexOf(info.Key);
                  if (idx >= 0) {
                    keys.splice(idx, 1);
                  }
                  ps.push(this._install(info.Category, info.Key));
                });
                keys.forEach((k) => {
                  errs.push({ key: k, err: "errors.depend.notExist" });
                });
                results.push(...errs);
                Promise.all(ps)
                  .then((lresults) => {
                    results.push(...lresults);
                    installFunc(levels[i++]);
                  })
                  .catch((_reasons) => {});
              });
          };
          let i = 0;

          installFunc(levels[i++]);
          return;
      }
    });
  }

  private _install(category: string, key: string): Promise<{}> {
    return new Promise<{}>((resolve, _reject) => {
      this._dependencyManager.getDependencyRun(category, key).then((run) => {
        let info = this._statusManager.getInstallInfo(key);
        if (!info || info.InstallStatus == InstallStatus.Uninstall) {
          this._statusManager.updateInstallStatus(
            run.Key,
            InstallStatus.Installing
          );
          this.runSteps(run)
            .then((_r) => {
              this._statusManager.updateInstallStatus(
                run.Key,
                InstallStatus.Installed,
                undefined,
                run.Version
              );
              resolve({ key: run.Key, err: "" });
            })
            .catch((reason) => {
              this._statusManager.updateInstallStatus(
                run.Key,
                InstallStatus.Uninstall
              );
              resolve({ key: run.Key, err: reason });
            });
          //  resolve({ key: run.Key, err: "" });
        }
      });
    });
  }
}
