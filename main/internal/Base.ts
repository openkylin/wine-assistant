/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { InfoBase, IpcProcess, KeyStep, Step } from "../commons/define";
//import { logger } from "../utils/Logger";
export interface Run {}

export abstract class BaseInternal extends IpcProcess {
  protected runSteps(appInfo: InfoBase): Promise<string> {
    return new Promise<string>((res, rej) => {
      if (appInfo.Steps.length == 0) {
        res("");
      } else {
        let i = 0;
        //let steps = appInfo.Steps.copyWithin(0);
        //let allResult = [] as any[];
        //通过递归的方式，实现顺序执行
        const processFunc = (step: Step) => {
          //只要顺利执行到最后一个，那么肯定全部成功，调用resolve
          if (step == undefined) {
            res("");
            return;
          }
          let keyStep: KeyStep = {
            Key: appInfo.Key,
            Name: appInfo.Name,
            Description: appInfo.Description,
            Icon: "",
          };
          step
            .process(keyStep)
            .then((result) => {
              //allResult.push(result);
              processFunc(appInfo.Steps[i++]);
            })
            .catch((reason) => {
              //任何异步执行异常，直接reject
              rej(reason);
            });
        };
        processFunc(appInfo.Steps[i++]);
      }
    });
  }
}
