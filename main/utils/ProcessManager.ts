/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { exec } from "child_process";
import * as os from "os";

interface ProcessInfo {
  name: string;
  pid: number;
}

export class ProcessManager {
  private user: string;
  private static instance: ProcessManager;

  constructor(user: string = os.userInfo().username) {
    this.user = user;
  }

  public static getInstance(): ProcessManager {
    if (!this.instance) {
      this.instance = new ProcessManager();
    }
    return this.instance;
  }

  public get_processes(): Promise<ProcessInfo[]> {
    const cmd = `ps -u ${this.user} -o pid,comm`;
    return new Promise((resolve, reject) => {
      exec(cmd, (err, stdout, _stderr) => {
        if (err) {
          reject(err);
          return;
        }

        const lines = stdout.split("\n");
        lines.shift(); // skip header line
        const processes: ProcessInfo[] = [];
        lines.forEach((line) => {
          const [pidStr, name] = line.trim().split(/\s+/);
          const pid = parseInt(pidStr, 10);
          if (!isNaN(pid)) {
            processes.push({ name, pid });
          }
        });

        resolve(processes);
      });
    });
  }

  public async wait_for_process(
    name: string,
    timeout: number = 30
  ): Promise<boolean> {
    const start = Date.now();
    while (Date.now() - start < timeout * 1000) {
      const processes = await this.get_processes();
      if (processes.some((p) => p.name === name)) {
        return true;
      }
      await new Promise((resolve) => setTimeout(resolve, 500));
    }
    return false;
  }
}

/* // 测试代码
(async () => {
  const pm = new ProcessManager("kylin");
  console.log(await pm.get_processes());
  await pm.wait_for_process("top");
  console.log("process is terminated");
})(); */
