/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import * as log4js from "log4js";
import * as path from "path";
import { WorkDirConfig } from "../commons/config";

const pl = {} as log4js.PatternLayout;
pl.type = "pattern";
pl.pattern = "[%d] [%p] [%f:%l] %m";
/*
 */

const config = {
  appenders: {},
  categories: {},
} as log4js.Configuration;

config.appenders["console"] = { type: "console", layout: pl };
config.appenders["file"] = {
  type: "file",
  maxLogSize: 10485760,
  filename: path.join(WorkDirConfig.WorkDir, "logs", "wine-assistant.log"), //"/var/log/wine-assistant.log",
  layout: pl,
};

let d = {
  appenders: ["console"],
  level: "DEBUG",
  enableCallStack: true,
};
let envLevel = log4js.levels.getLevel(
  process.env.WINE_ASSISTANT_LOGLEVEL || ""
);

if (!import.meta.env.DEV) {
  d = {
    appenders: ["file"],
    level: envLevel?.levelStr || "INFO",
    enableCallStack: true,
  };
}

config.categories = {
  default: d,
};
config.pm2 = true;

log4js.configure(config);
/**
 * example:
 * logger.info("hello logger")
 */
export const logger = log4js.getLogger("default");
