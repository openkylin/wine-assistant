/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { WineCommand } from "./WineCommand";

export class Uninstaller extends WineCommand {
  public constructor(container?: string) {
    super(
      "uninstaller.exe",
      { WINEDLLOVERRIDES: "mscoree=  mshtml=", WINEDEBUG: "-all" },
      undefined,
      container
    );
  }
  public list(): Promise<{ [key: string]: string }> {
    return new Promise<{ [key: string]: string }>((resolve, reject) => {
      this._config.Args = "--list";
      this.run(true).then((out) => {
        let lines = out.replaceAll("\r", "").split("\n");
        let result = {};
        lines.forEach((line) => {
          if (line.indexOf("|||") <= 0) {
            return;
          }
          let arr = line.split("|||");
          if (arr.length == 2) {
            result[arr[0]] = arr[1];
          }
        });
        resolve(result);
        //resolve(out.split("\n"));
      }, reject);
    });
  }
  public panel() {
    this._config.Args = "";
    this.run();
  }
  public remove(guid: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      this._config.Args = `--remove "${guid}"`;
      this.run(true).then(resolve, reject);
    });
  }
}
