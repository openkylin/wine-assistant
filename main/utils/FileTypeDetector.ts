/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import * as path from "path";
import * as fs from "fs";
import * as rimraf from "rimraf";
import * as mkdirp from "mkdirp";
import { exec } from "child_process";
import * as fileType from "file-type";
import { escapeLinuxPath } from "./Utils";
import { logger } from "./Logger";

interface ArchiveCommand {
  command: string;
  args: string[];
}

interface ArchiveCommands {
  [ext: string]: ArchiveCommand;
}

export class FileTypeDetector {
  private readonly filePath: string;
  private readonly forceExt: string | undefined;
  constructor(filePath: string, forceExt?: string) {
    this.filePath = filePath;
    this.forceExt = forceExt;
  }

  /**
   * 解压缩文件到指定目录
   *
   * @param destDir 目标目录，必须为绝对路径
   * @param callback 可选的回调函数，用于在解压缩完成后执行其他操作
   */
  public detector1(destDir: string, callback?: () => void): Promise<string> {
    return new Promise<string>((res, rej) => {
      // 验证输入参数
      if (!path.isAbsolute(destDir)) {
        logger.error("Invalid destination directory");
        rej("Invalid destination directory");
        return;
      }

      // 判断文件类型
      fileType
        .fromFile(this.filePath)
        .then((type) => {
          if (!type) {
            logger.error("Unknown file type");
            rej("Unknown file type");
            return;
          }

          // 如果文件类型不是压缩包，则直接返回
          if (!this.isArchiveType(type.ext) && !this.forceExt) {
            res("Not an archive file");
            return;
          }

          // 删除目标目录
          if (fs.existsSync(destDir)) {
            rimraf.sync(destDir);
          }

          // 创建目标目录
          mkdirp.sync(destDir);

          // 定义解压缩命令
          const commands: ArchiveCommands = {
            zip: {
              command: "unzip",
              args: [
                "-O",
                "GBK",
                "-o",
                "-q",
                escapeLinuxPath(this.filePath),
                "-d",
                escapeLinuxPath(destDir),
              ],
            },
            rar: {
              command: "unrar",
              args: [
                "e",
                "-y",
                escapeLinuxPath(this.filePath),
                escapeLinuxPath(destDir),
              ],
            },
            "7z": {
              command: "7z",
              args: [
                "x",
                "-y",
                escapeLinuxPath(this.filePath),
                `-o${escapeLinuxPath(destDir)}`,
              ],
            },
            cab: {
              command: "cabextract",
              args: [
                "-d",
                escapeLinuxPath(destDir),
                "-q",
                escapeLinuxPath(this.filePath),
              ],
            },
            gz: {
              command: "tar",
              args: [
                "-xf",
                escapeLinuxPath(this.filePath),
                "-C",
                escapeLinuxPath(destDir),
              ],
            },
            xz: {
              command: "tar",
              args: [
                "-xf",
                escapeLinuxPath(this.filePath),
                "-C",
                escapeLinuxPath(destDir),
              ],
            },
            bz2: {
              command: "bzip2",
              args: [
                "-d",
                escapeLinuxPath(this.filePath),
                "-c",
                "|",
                "tar",
                "-xf",
                "-",
                "-C",
                escapeLinuxPath(destDir),
              ],
            },
          };

          // 根据文件类型执行相应的解压缩命令
          let command = commands[type.ext];
          if (this.forceExt) {
            command = commands[this.forceExt];
          }
          if (command) {
            logger.debug(`${command.command} ${command.args.join(" ")}`);
            exec(`${command.command} ${command.args.join(" ")}`, (err) => {
              if (err) {
                logger.error("Error:", err);
              } else {
                logger.debug("Extracted:", this.filePath);

                if (callback) {
                  callback();
                }
                res("Extracted ok");
              }
            });
          } else {
            const errmsg = `Unsupported file type: ${
              this.forceExt ? this.forceExt : type.ext
            }`;
            logger.error(errmsg);
            rej(errmsg);
          }
        })
        .catch((error: any) => {
          logger.error("Error:", error);
          rej(`Error: ${error}`);
        });
    });
  }

  /**
   * 判断文件类型是否为压缩包类型
   *
   * @param ext 文件类型后缀名
   */
  private isArchiveType(ext: string): boolean {
    return ["zip", "rar", "7z", "cab", "gz", "xz", "bz2"].includes(ext);
  }
}
