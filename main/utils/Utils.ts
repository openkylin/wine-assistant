/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import crypto from "crypto";
import { WorkDirConfig } from "../commons/config";
import * as fs from "fs";
import * as path from "path";
import { logger } from "./Logger";
import { ExportIcon } from "./IconUtil";
import { app } from "electron";

export class Crypto {
  static md5(data: any): string {
    let hash = crypto.createHash("md5");
    return hash.update(data).digest("hex");
  }
}

export function generateDesktop(
  key: string,
  name: string,
  comment: string,
  cmd: string,
  programPath: string,
  iconUrl?: string
): Promise<void> {
  return new Promise<void>(async (resolve, reject) => {
    let desktopName = `${name}.desktop`;

    const saveFile = (iconPath: string) => {
      const desktopFileContent = `[Desktop Entry]
Type=Application
StartupNotify=true
Name=${name}
Comment=${comment}
Exec=${cmd}
Icon=${iconPath}
Keywords=KylinWine
Categories=KylinWine
Terminal=false
`;
      let appsPath = path.join(WorkDirConfig.ApplicationsDir, desktopName);
      let desktopPath = path.join(WorkDirConfig.DesktopDir, desktopName);

      try {
        if (fs.existsSync(desktopPath)) {
          fs.unlinkSync(desktopPath);
        }
        if (fs.existsSync(appsPath)) {
          fs.unlinkSync(appsPath);
        }
        fs.writeFileSync(desktopPath, desktopFileContent, { mode: 0o755 });
        // fs.writeFileSync(appsPath, desktopFileContent, { mode: 0o755 });
        logger.debug("on create", cmd, iconPath);
      } catch (error) {
        logger.error("on create", error);
      }
    };

    if (iconUrl !== undefined && iconUrl !== "" && iconUrl !== null) {
      ExportIcon.downloadIcon(iconUrl, key, programPath).then((icon) => {
        logger.info("-------------------------------", icon);
        saveFile(icon);
        resolve();
      });
    } else {
      ExportIcon.getIcon(programPath, key).then(
        (iconPath) => {
          saveFile(iconPath);
          resolve();
        },
        (reason) => {
          logger.error("Get icon fail:" + reason);
          saveFile("");
          resolve();
        }
      );
    }
  });
}

//根据传入的参数决定生成快捷方式的位置（Desktop/桌面/开始菜单）
export function generateShortcut(
  bottle: string,
  name: string,
  cmd: string,
  programPath: string,
  folder: string[]
): Promise<void> {
  return new Promise<void>(async (resolve, reject) => {
    let desktopName = `${name}.desktop`;

    const saveFile = (iconPath: string) => {
      const desktopFileContent = `[Desktop Entry]
Type=Application
StartupNotify=true
Name=${name}
Exec=${cmd}
Icon=${iconPath}
Terminal=false
`;
      let appsPath = path.join(WorkDirConfig.ApplicationsDir, desktopName);
      let desktopPath = "";
      if (fs.existsSync(path.join(WorkDirConfig.HomeDir, "桌面"))) {
        desktopPath = path.join(WorkDirConfig.HomeDir, "桌面", desktopName);
      } else if (fs.existsSync(path.join(WorkDirConfig.HomeDir, "Desktop"))) {
        desktopPath = path.join(WorkDirConfig.HomeDir, "Desktop", desktopName);
      }
      try {
        if (fs.existsSync(desktopPath)) {
          fs.unlinkSync(desktopPath);
        }
        if (fs.existsSync(appsPath)) {
          fs.unlinkSync(appsPath);
        }
        if (folder.includes("1")) {
          fs.writeFileSync(desktopPath, desktopFileContent, { mode: 0o755 });
        }
        if (folder.includes("2")) {
          fs.writeFileSync(appsPath, desktopFileContent, { mode: 0o755 });
        }
        logger.debug("on create", cmd, iconPath);
      } catch (error) {
        logger.error("on create", error);
      }
    };

    ExportIcon.getIcon(programPath, undefined, bottle).then(
      (iconPath) => {
        saveFile(iconPath);
        resolve();
      },
      (reason) => {
        saveFile("wine-assistant");
        logger.error("Get icon fail:" + reason);
        resolve();
      }
    );
  });
}

export function getIconPath(filePath: string): string {
  try {
    if (fs.existsSync(filePath)) {
      var lines = fs.readFileSync(filePath, "utf8").split("\n");
      for (var i in lines) {
        if (
          lines[i].indexOf("Icon") !== -1 &&
          lines[i].length > "Icon= ".length
        ) {
          return lines[i].split("=")[1].trim();
        }
      }
    }
  } catch (error) {
    logger.error(error);
  }
  return "";
}

export function removeDesktop(name: string) {
  let desktopName = `${name}.desktop`;

  let appsPath = path.join(WorkDirConfig.ApplicationsDir, desktopName);
  let desktopPath = path.join(WorkDirConfig.DesktopDir, desktopName);
  try {
    var iconPath = "";
    if (fs.existsSync(desktopPath)) {
      iconPath = getIconPath(desktopPath);
      fs.unlinkSync(desktopPath);
    }
    if (fs.existsSync(appsPath)) {
      if (iconPath == "") iconPath = getIconPath(appsPath);
      fs.unlinkSync(appsPath);
    }

    if (fs.existsSync(iconPath)) {
      fs.unlinkSync(iconPath);
    }
  } catch (error) {
    logger.error(error);
  }
}

export function loadConfig(filePath: string): string {
  let data = "";
  try {
    if (fs.existsSync(filePath)) {
      data = fs.readFileSync(filePath, "utf8");
    }
  } catch (error) {}
  return data;
}
export function saveConfig(filePath: string, data: string) {
  let fileDir = path.dirname(filePath);
  try {
    //logger.log(filePath, data);
    if (!fs.existsSync(fileDir)) {
      fs.mkdirSync(fileDir, { recursive: true });
    }
    fs.writeFileSync(filePath, data);
  } catch (error) {
    //logger.log(error);
  }
}

export function renameFile(oldPath: string, newPath: string) {
  try {
    const newDir = path.dirname(newPath);
    if (!fs.existsSync(newDir)) {
      fs.mkdirSync(newDir, { recursive: true });
    }
    fs.renameSync(oldPath, newPath);
    logger.log(`File ${oldPath} renamed to ${newPath}`);
  } catch (err: any) {
    logger.error(err.message);
  }
}

export function isDirectory(path: string): boolean {
  try {
    const stats = fs.statSync(path);
    return stats.isDirectory();
  } catch (err: any) {
    logger.error(err.message);
    return false;
  }
}

export function fileExists(path: string): boolean {
  try {
    fs.accessSync(path, fs.constants.F_OK);
    return true;
  } catch (err) {
    return false;
  }
}
/**
 * 将 Linux 风格的路径转义，防止在命令行执行时出现错误。
 * @param path 要转义的 Linux 风格的路径。
 * @returns 转义后的安全字符串。
 */
export function escapeLinuxPath(path: string): string {
  const specialChars = [
    " ",
    "\t",
    "\n",
    "\r",
    "\f",
    "\v",
    ";",
    "&",
    "|",
    "<",
    ">",
    "(",
    ")",
    "$",
    "`",
    "\\",
    '"',
    "'",
  ];

  let escapedPath = "";
  for (let i = 0; i < path.length; i++) {
    const char = path[i];
    if (specialChars.includes(char)) {
      escapedPath += `\\${char}`;
    } else {
      escapedPath += char;
    }
  }

  return escapedPath;
}

/**
 * 删除一个链接及其真实文件。
 * @param filePath 文件或链接的路径。
 */
export function deleteFileOrLink(filePath: string): void {
  try {
    const stats = fs.lstatSync(filePath);

    if (stats.isSymbolicLink()) {
      // 如果是链接，则删除链接及其真实文件
      const realPath = fs.realpathSync(filePath);
      fs.unlinkSync(filePath);
      fs.unlinkSync(realPath);
    } else if (stats.isFile()) {
      // 如果是文件，则直接删除
      fs.unlinkSync(filePath);
    }
  } catch (error: any) {
    if (error.code !== "ENOENT") {
      // 如果文件不存在，则直接返回
      logger.error(`${error.message}`);
    }
  }
}

export function title(): [boolean, string] {
  let lang = app.getLocale();
  let titleName = "";
  let zh = false;
  if (lang === "zh-CN" || lang === "zh") {
    zh = true;
    if (WorkDirConfig.BuildVersion === "Commercial") {
      titleName = "麒麟 Wine 助手";
    } else {
      titleName = "openKylin  Wine 助手";
    }
  } else {
    if (WorkDirConfig.BuildVersion === "Commercial") {
      titleName = "Kylin Wine Assistant";
    } else {
      titleName = "openKylin Wine Assistant";
    }
  }
  return [zh, titleName];
}
