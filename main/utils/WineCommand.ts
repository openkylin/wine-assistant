/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { spawn } from "child_process";
import * as fs from "fs";
import * as Path from "path";
import * as iconv from "iconv-lite";
import { WorkDirConfig } from "../commons/config";
import { logger } from "./Logger";
import { escapeLinuxPath } from "./Utils";
//import { ChildProcess } from 'child_process';

/**
 * Wine命令启动配置
 */
export class WineConfig {
  Env: { [key: string]: string } = {};
  Cmd: string = "";
  Args: string = "";
  Container: string = "";
}

/**
 * Wine命令处理类，负责容器初始化以及执行相关命令
 */
export class WineCommand {
  protected _config: WineConfig;
  /**
   * 定义默认的容器路径
   */
  private static _container_path: string = WorkDirConfig.ContainerDir;
  private _wine_: string = WorkDirConfig.Wine;
  //private _childProcess: ChildProcess | undefined;

  /**
   * 创建一个winecommand
   *
   * @param cmd 要执行的命令，例如winecfg.exe
   * @param env 要传递给wine的环境变量
   * @param args exe 程序后面带的参数
   * @container 容器名称
   */
  public constructor(
    cmd: string,
    env?: { [key: string]: string },
    args?: string,
    container?: string,
    useStart?: boolean,
    runInWindow?: boolean | string
  ) {
    this._config = new WineConfig();
    this._config.Cmd = cmd;
    /*
    logger.debug(
      "xwtest------",
      this._config.Cmd,
      fs.existsSync(this._config.Cmd),
      fs.statSync(this._config.Cmd)
    );
    */
    if (container) {
      if (container.trim().length === 0) {
        container = "default";
      }
      this._config.Container = Path.join(
        WineCommand._container_path,
        container
      );
    } else {
      this._config.Container = Path.join(
        WineCommand._container_path,
        "default"
      );
    }

    if (!fs.existsSync(this._config.Container)) {
      fs.mkdirSync(this._config.Container, { recursive: true });
    }

    if (env) {
      if (!env.hasOwnProperty("WINEPREFIX")) {
        this._config.Env = { ...env, WINEPREFIX: `${this._config.Container}` };
      } else {
        this._config.Env = env;
      }
    } else {
      this._config.Env = { WINEPREFIX: `${this._config.Container}` };
    }

    if (args) {
      this._config.Args = args;
    }

    if (useStart && Path.isAbsolute(cmd)) {
      var exe = Path.basename(cmd);
      var dospath = WineCommand.unix2dosPath(Path.dirname(cmd), container);
      this._config.Cmd = "start";
      //this._config.Args = `/d "${dospath}" "${exe}"` + " " + this._config.Args;
      let newArgs = `/d "${dospath}" `;
      if (runInWindow) {
        let resolution = "0x0";
        if (typeof runInWindow == "string") {
          let wh = runInWindow.split("x");
          if (wh.length == 2) {
            if (
              Number.parseInt(wh[0]) >= 800 &&
              Number.parseInt(wh[1]) >= 600
            ) {
              resolution = `${wh[0]}x${wh[1]}`;
            }
          }
        }
        newArgs += ` explorer /desktop=wine-assistant,${resolution} `;
      }
      newArgs += ` "${exe}"`;
      this._config.Args = newArgs + " " + this._config.Args;
    }
  }

  /**
   * 从桌面文件路径中获取可执行文件路径
   *
   * @param {string} desktopFilePath - 桌面文件路径
   * @returns {string} 可执行文件路径
   */
  private getExecutablePathFromDesktopFile(desktopFilePath: string): string {
    const desktopFileContent = fs.readFileSync(desktopFilePath, {
      encoding: "utf-8",
    });
    const lines = desktopFileContent.split("\n");
    for (const line of lines) {
      if (line.startsWith("Exec=")) {
        const command = line.slice(line.indexOf("=") + 1).trim();
        return command;
      }
    }
    return "";
  }

  public buildCommand(): [string, string] {
    if (!this._config?.Cmd?.trim()) {
      return [`Invalid command: ${this._config?.Cmd}`, ""];
    }

    // 路径或者wine内置命令，如regedit.exe
    if (
      !fs.existsSync(this._config.Cmd.trim()) &&
      (this._config.Cmd.includes("/") || this._config.Cmd.includes("\\"))
    ) {
      return [`Command not found: ${this._config.Cmd}`, ""];
    }

    let envVars = Object.entries(this._config.Env)
      .map(([key, value]) => `${key}="${value}"`)
      .join(" ");

    envVars = envVars + " LANG=zh_CN.UTF-8 ";
    if (this._config.Env["WINEDEBUG"] === undefined) {
      if (!import.meta.env.DEV && process.env.WINE_ASSISTANT_DEBUG != "true") {
        envVars = envVars + " WINEDEBUG=-all ";
      }
    }

    let windowPath = Path.join(
      WorkDirConfig.ContainerDir,
      WorkDirConfig.ContainerName,
      "drive_c",
      "windows"
    );

    let Syswow64Path = Path.join(windowPath, "syswow64");

    if (!fs.existsSync(Syswow64Path) && fs.existsSync(windowPath)) {
      envVars = envVars + " WINEARCH=win32 ";
    }
    //envVars = envVars + " WINEDEBUG=-all,+shell ";

    const cmdWithArgs = this._config.Cmd.trim().endsWith(".desktop")
      ? this.getExecutablePathFromDesktopFile(this._config.Cmd)
      : `env ${envVars} ${this._wine_} ${escapeLinuxPath(this._config.Cmd)} ${
          this._config.Args
        }`;

    logger.debug(`Wine command: ${cmdWithArgs}`);
    if (cmdWithArgs.trim() === "") {
      return [`Invalid command: ${cmdWithArgs}`, ""];
    }

    return ["", cmdWithArgs];
  }

  public run(resultStr?: boolean): Promise<string> {
    return new Promise((resolve, reject) => {
      const [errorMsg, cmdWithArgs] = this.buildCommand();
      if (errorMsg !== "") {
        reject(errorMsg);
        return;
      }
      logger.debug(`Running Wine command: ${cmdWithArgs}`);
      logger.debug(
        `Environment variables: ${JSON.stringify(this._config.Env)}`
      );
      logger.debug(`Container: ${this._config.Container}`);
      let stdios = [null, null, null] as any[];
      let resultString: string = "";
      const proc = spawn(cmdWithArgs, {
        shell: true,
        stdio: stdios,
      });
      //this._childProcess = proc; // 保存子进程的引用
      proc.stdout.on("data", (data) => {
        logger.debug(`${iconv.decode(data, "gbk").replaceAll("\n", "")}`);
        if (resultStr) resultString = resultString + iconv.decode(data, "gbk");
      });

      proc.stderr.on("data", (data) => {
        // logger.error(`${iconv.decode(data, "gbk").replaceAll("\n", "")}`);
        if (resultStr) resultString = resultString + iconv.decode(data, "gbk");
      });

      proc.on("error", (error) => {
        logger.error(`Shell failed: ${error.message}`);
      });

      proc.on("exit", (code) => {
        if (code !== 0) {
          logger.debug(`Wine command failed with code ${code}`);
          resolve(`Wine command with code ${code}`);
          //proc.kill();
        } else {
          resolve(resultString);
          //reject("Exec wine command fail")
        }
      });
    });
  }

  static dos2unixPath(dos: string, container?: string): string {
    var con = container ? container : "default";
    // 将 Windows 路径转换为 Unix 路径
    let unixPath = Path.win32.normalize(dos).replace(/\\/g, "/");

    // 去除盘符和冒号前的内容，例如："C:\Program Files" => "Program Files"
    unixPath = unixPath.replace(/^[a-zA-Z]:/, "").replace(/^\//, "");

    // 将空格转换为 "\ "
    //unixPath = unixPath.replace(/ /g, "\\ ");
    // unixPath = escapeLinuxPath(unixPath);

    return Path.join(WineCommand._container_path, con, "drive_c", unixPath);
  }

  public static unix2dosPath(unixPath: string, container?: string): string {
    var con = container ? container : "default";
    var dosPath: string = "";
    var driverRoot = Path.join(WineCommand._container_path, con, "drive_c");
    if (Path.isAbsolute(unixPath)) {
      if (unixPath.indexOf(driverRoot) === 0) {
        dosPath = Path.join("C:", unixPath.substring(driverRoot.length));
      } else {
        /**
         * TODO: 除了z: 之外，还有其他可能，暂时只返回z:
         */
        dosPath = Path.join("z:", unixPath);
      }
      dosPath = dosPath.replace(/\//g, "\\");
    }

    logger.debug(`driverRoot: ${driverRoot}`);
    logger.debug(`unixPath: ${unixPath}`);
    logger.debug(`dosPath: ${dosPath}`);

    return dosPath;
  }
  /*
  public kill(): void {
    logger.debug("<ct-test>:jump in wineCmd.kill");
    if (this._childProcess) {
      this._childProcess.kill();
      this._childProcess = undefined;
    }
  }
  */
}
