/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import * as Path from "path";
import * as fs from "fs";
import chokidar from "chokidar";
import { WorkDirConfig } from "../commons/config";
import { fileExists, deleteFileOrLink } from "../utils/Utils";
import { WineCommand } from "./WineCommand";
import { IpcManager } from "../managers/IpcManager";
import { logger } from "../utils/Logger";
import { ContainerStatus } from "../commons/define";

/**
 * Wineboot 操作类
 */
export class Wineboot extends WineCommand {
  private static _execWatcher: chokidar.FSWatcher;
  /**
   * Usage: wineboot [options]
   * Options:
   *   -h, --help           Display this help message
   *   -e, --end-session    End the current session cleanly
   *   -f, --force          Force exit for processes that don't exit cleanly
   *   -i, --init           Perform initialization for first Wine instance
   *   -k, --kill           Kill running processes without any cleanup
   *   -r, --restart        Restart only, don't do normal startup operations
   *   -s, --shutdown       Shutdown only, don't reboot
   *   -u, --update         Update the wineprefix directory
   */
  public constructor(container?: string) {
    super(
      WorkDirConfig.Wineboot,
      { WINEDLLOVERRIDES: "mscoree=  mshtml=" },
      undefined,
      container
    );
  }
  public static IsInitialized() {
    const winebootPath = Path.join(
      WorkDirConfig.ContainerDir,
      WorkDirConfig.ContainerName,
      "drive_c",
      "windows",
      "system32",
      "wineboot.exe"
    );
    return fileExists(winebootPath);
  }

  public static async checkAndInitContainer() {
    const winebootPath = Path.join(
      WorkDirConfig.ContainerDir,
      WorkDirConfig.ContainerName,
      "drive_c",
      "windows",
      "system32",
      "wineboot.exe"
    );
    const sendContainerStatus = (status: string) => {
      IpcManager.sendToView("Container", {
        ChangedStatus: "ContainerStatus",
        Status: status,
        ContainerName: WorkDirConfig.ContainerName,
      });
    };
    const createDir = () => {
      logger.debug(
        WorkDirConfig.ProtableDir,
        fs.existsSync(WorkDirConfig.ProtableDir)
      );
      if (!fs.existsSync(WorkDirConfig.ProtableDir)) {
        fs.mkdirSync(WorkDirConfig.ProtableDir, { recursive: true });
      }
      if (!fs.existsSync(WorkDirConfig.DownloadingDir)) {
        fs.mkdirSync(WorkDirConfig.DownloadingDir, { recursive: true });
      }
      if (!fs.existsSync(WorkDirConfig.DownloadedDir)) {
        fs.mkdirSync(WorkDirConfig.DownloadedDir, { recursive: true });
      }
      if (!fs.existsSync(WorkDirConfig.ConfigDir)) {
        fs.mkdirSync(WorkDirConfig.ConfigDir, { recursive: true });
      }
      if (!fs.existsSync(WorkDirConfig.TrashDir)) {
        fs.mkdirSync(WorkDirConfig.TrashDir, { recursive: true });
      }
      if (!fs.existsSync(WorkDirConfig.IconsDir)) {
        fs.mkdirSync(WorkDirConfig.IconsDir, { recursive: true });
      }
      deleteFileOrLink(
        Path.join(
          WorkDirConfig.ContainerDir,
          WorkDirConfig.ContainerName,
          "drive_c",
          "windows",
          "system32",
          "winemenubuilder.exe"
        )
      );
      deleteFileOrLink(
        Path.join(
          WorkDirConfig.ContainerDir,
          WorkDirConfig.ContainerName,
          "drive_c",
          "windows",
          "syswow64",
          "winemenubuilder.exe"
        )
      );
    };
    if (!fileExists(winebootPath)) {
      deleteFileOrLink(
        Path.join(
          WorkDirConfig.ContainerDir,
          WorkDirConfig.ContainerName,
          ".update-timestamp"
        )
      );
      sendContainerStatus("Initializing");
      let wb = new Wineboot(WorkDirConfig.ContainerName);
      wb.init()
        .then((resolve) => {
          logger.info("Container Initialized");
          createDir();
          sendContainerStatus(ContainerStatus.Initialized);
          this._execWatcher = chokidar.watch([winebootPath], {
            followSymlinks: false,
          });

          this._execWatcher.on("unlink", (file) => {
            sendContainerStatus(ContainerStatus.Deleted);
            logger.log("--------ContainerDeleted--------");
          });
        })
        .catch((reason) => {
          logger.error(reason);
          sendContainerStatus(ContainerStatus.InitializationFailed);
        });
    } else {
      createDir();
      sendContainerStatus(ContainerStatus.Initialized);
      this._execWatcher = chokidar.watch([winebootPath], {
        followSymlinks: false,
      });

      this._execWatcher.on("unlink", (file) => {
        sendContainerStatus(ContainerStatus.Deleted);
        logger.log("--------ContainerDeleted--------");
      });
    }
  }

  public init(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._config.Args = "-i";
      this.run().then(resolve, reject);
    });
  }

  public update(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._config.Args = "-u";
      this.run().then(resolve, reject);
    });
  }

  public shutdown(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._config.Args = "-s";
      this.run().then(resolve, reject);
    });
  }

  public restart(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._config.Args = "-r";
      this.run().then(resolve, reject);
    });
  }

  public kill(): Promise<string> {
    return new Promise((resolve, reject) => {
      this._config.Args = "-k";
      this.run().then(resolve, reject);
    });
  }
}

/**
 * 测试代码
 */
// const wineboot = new Wineboot();
// wineboot.init(); // 运行 wineboot -i 命令

// const wineboot2 = new Wineboot("my-container");
// wineboot2.update(); // 在容器 "my-container" 中运行 wineboot -u 命令

// const wineboot3 = new Wineboot();
// wineboot3.shutdown(); // 运行 wineboot -s 命令

// const wineboot4 = new Wineboot();
// wineboot4.restart(); //
