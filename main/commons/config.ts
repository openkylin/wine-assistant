/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import os from "os";
import fs from "fs-extra";
import path from "path";
import { execSync } from "child_process";

class _WorkDirConfig {
  BuildVersion: string = "Community"; // Commercial 商业版 or Community
  HomeDir: string = os.homedir();
  WorkDir: string = path.join(this.HomeDir, ".wine-assistant");
  ContainerDir: string = path.join(this.HomeDir, ".okylin-wine");
  DebuildDir: string = path.join(this.WorkDir, "kylin-wine-bottle");
  ContainerConfig: string = path.join(this.WorkDir, "container.json");
  ContainerName: string = "default";
  Wine: string;
  Wineboot: string = "wineboot";
  WineKill: string = "/usr/bin/winekillall";
  AppWarehouse: string = "win-program";
  ConfYml: string = "release.yml";
  RepoTemplate: string;

  ProtableDir: string = path.join(
    this.ContainerDir,
    this.ContainerName,
    "drive_c",
    "Program Protable"
  );
  TrashDir: string = path.join(this.WorkDir, "trash");
  DownloadingDir: string = path.join(this.WorkDir, "downloading");
  DownloadedDir: string = path.join(this.WorkDir, "downloaded");
  //DownloadConfig: string = path.join(this.DownloadDir, "config.json");
  ConfigDir: string = path.join(this.WorkDir, "config");

  InstallStatusConfig: string = path.join(
    this.ConfigDir,
    "install_status.json"
  );
  ContainerInstallInfo: string = path.join(
    this.ConfigDir,
    this.ContainerName + ".json"
  );

  DownloadStatusConfig: string = path.join(
    this.ConfigDir,
    "download_status.json"
  );
  ApplicationsDir: string = path.join(
    this.HomeDir,
    ".local/share/applications"
  );
  LocalInstallConfig: string = path.join(this.ConfigDir, "local_install.json");
  LocalStartupConfig: string = path.join(this.ConfigDir, "local_startup.json");
  IconsDir: string = path.join(this.WorkDir, "icons");
  DesktopDir: string = "";
  constructor() {
    let userDirsPath = path.join(this.HomeDir, ".config", "user-dirs.dirs");
    let r = execSync(". " + userDirsPath + "&& echo $XDG_DESKTOP_DIR");
    let url = "";
    let resource = "";
    this.DesktopDir = r.toString().replaceAll("\n", "");
    if (this.BuildVersion.includes("Commercial")) {
      this.Wine = "/usr/bin/kylin-wine";
      url = "https://archive2.kylinos.cn";
      resource = "/wine-assistant/config/#REPO#/#BRANCH#/#FILEPATH#";
    } else {
      this.Wine = "/usr/bin/okylin-wine";
      url = "https://gitee.com/openkylin";
      resource = "/#REPO#/raw/#BRANCH#/#FILEPATH#";
    }
    let repocfg = path.join(this.ConfigDir, "source.cfg");
    try {
      if (fs.existsSync(repocfg)) {
        let data = fs.readFileSync(repocfg, "utf8");
        let regex = /^AppSrc=(\S+)/m;
        let match = data.match(regex);
        if (match) {
          // 匹配成功，match[1]将包含等号后面的值
          if (match[1] !== "") {
            url = match[1].replace(
              new RegExp(resource.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"), "g"),
              ""
            );
          }
        }
      } else {
        fs.writeFileSync(repocfg, `AppSrc=${url}`);
      }
    } catch (error) {}
    this.RepoTemplate = `${url}${resource}`;
  }
}

export const WorkDirConfig = new _WorkDirConfig();
