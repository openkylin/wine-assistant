/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
export enum ActionEnum {
  Start = "start",
  Pause = "pause",
  Resume = "resume",
  Delete = "delete",
}
export enum DownloadStatus {
  Undownload = "Undownload",
  DownloadPaused = "DownloadPaused",
  Downloading = "Downloading",
  Downloaded = "Downloaded",
}
export enum InstallStatus {
  Uninstall = "Uninstall",
  Installing = "Installing",
  Installed = "Installed",
}

export enum ContainerStatus {
  UnInitialization = "Uninitialization",
  Initializing = "Initializing",
  Initialized = "Initialized",
  InitializationFailed = "Initialization failed",
  Deleted = "Deleted",
}
export enum DepStatus {
  Uninstall = "Uninstall",
  Installed = "Installed",
  Update = "Update",
}

export interface DownloadFile {
  FileKey: string;
  FileName: string;
  Progress: number;
  DownloadStatus: DownloadStatus;
}
export interface UninstallInfo {
  UninstallKey: string; //如果IsProtable为true，值为安装路径，否则为guid
  DesktopName: string;
  DisplayName: string;
}
//export type FileStatus = DownloadStatus | InstallStatus;
export interface InstallInfo {
  DownloadFiles: { [filenkey: string]: string };
  //DownloadStatus: DownloadStatus;
  //DownloadProgress: number; //Undownload值为0，Downloaded值为100，其它情况值为(0,100)
  InstallStatus: InstallStatus;
  ExecFilePath: string | undefined;
  IsProtable: boolean;
  UninstallInfo: UninstallInfo;
}

export enum GradeEnum {
  Unknown = "Unknown",
  Fail = "Fail",
  Bronze = "Bronze",
  Silver = "Silver",
  Gold = "Gold",
}
export enum WinArchEnum {
  win32 = "win32",
  win64 = "win64",
}
export enum LinuxArchEnum {
  x86_64 = "x86_64",
  aarch64 = "aarch64",
}
export enum ProgramCategoryEnum {
  Software = "Software",
  Games = "Games",
}
export enum FileTypeEnum {
  Exe = "exe",
  Msi = "msi",
  Zip = "zip",
  Rar = "rar",
  _7z = "7z",
}
export enum DependencyCategoryEnum {
  Essentials = "Essentials",
  Fonts = "Fonts",
}
export type Grade = "" | "Fail" | "Bronze" | "Silver" | "Gold"; //"" | GradeEnum;
export type WinArch = "" | WinArchEnum;
export type LinuxArch = "" | LinuxArchEnum;
export type ProgramCategory = "" | ProgramCategoryEnum;
export type FileType = "" | FileTypeEnum;
export type DependencyCategory = "" | DependencyCategoryEnum;

export interface IpcProtocol {
  Class: string; //调用接口的class，统一通过IpcManager注册管理
  Action: string; //调用接口的Class->Action
  Data: any; //调用实际处理函数的接口参数，可定义任意类型，每个Class->Action直接进行参数解析
}
export interface IpcResult extends IpcProtocol {
  Code: number; //小于0，表示错误
}

/**
 * 全局Ipc处理类
 */
export abstract class IpcProcess {
  //abstract processName: string;
  /**
   * 定义一个全局抽象的Ipc处理函数，用于处理渲染进程的请求
   * @param action 调用函数，每个manager自定义相关action
   * @param param 参数
   */
  abstract ipcProcess(action: string, param: any): Promise<any>;
  distory() {}
}
export interface WineAssistantConfig {
  RepoTemplate: string;
}

export interface OpDownloadParam {
  AppKey: string;
  FileKey: string;
  Action: ActionEnum;
}

export interface ParamInternal {
  Key: string;
  Category: ProgramCategory | DependencyCategory;
  RealUrl?: string;
  Extend?: any;
  Name?: string;
  Bottle?: string;
}

export interface IpcPort {
  readonly portMain: MessagePort;
  readonly portRenderer: MessagePort;
}
//每个step需要实现step接口
export abstract class Step {
  abstract action: string;
  /**
   * Setp自己的处理函数，每个在配置文件中定义的step，都要有对应的具体对象，并实现process
   * Manager在读取应用的配置文件是，会自动解析并通过Object.assign构造出Step对象的示例，供Internal调用
   */
  abstract process(param?: any): Promise<string>;
}

export interface InternalResult {
  Code: number; //执行结果,如果小于0表示错误
  Action: string; //对应执行动作，如果为安装，则为step中定义的action;如果为启动则写死为run
  Message: string; //错误信息,如果小于0，记录错误信息
}

export abstract class Executable {
  name: string = "";
  path: string = "";
  arguments: string = "";
  wineenv: any = {};
  run_in_window: boolean | string = false;
  /**
   * Setp自己的处理函数，每个在配置文件中定义的step，都要有对应的具体对象，并实现process
   * Manager在读取应用的配置文件是，会自动解析并通过Object.assign构造出Step对象的示例，供Internal调用
   */
  abstract exec(execPath?: string): Promise<string>;
}

export class InfoBase {
  public Key: string = "";
  public Name: string = "";
  public Description: string = "";
  public DetailUrl: string = "";
  public Dependencies: string[] = [] as string[];
  public Steps: Step[] = [];
}

export class ProgramInfo extends InfoBase {
  public Category: ProgramCategory = "";
  public Grade: Grade = "";
  public Icon: string = "";
  public Arch: WinArch = "";
  public LinuxArch: LinuxArch[] = [] as LinuxArch[];
  public IsInstalled: boolean = false;
  public Publish: boolean = false;
}

export class ProgramRun extends ProgramInfo {
  public Executable: Executable | undefined = undefined;
}

export class DependencyInfo extends InfoBase {
  Category: DependencyCategory = "";
}

export class DependencyRun extends DependencyInfo {
  Provider: string = "";
  License: string = "";
  License_url: string = "";
  //Dependencies: string[] = [] as string[];
  Conflicts: string[] = [] as string[];
  Hard_Dependencies?: string[] = [] as string[];
  Version: string = "";
  //Steps: Step[] = [];
}

export class KeyStep {
  Key: string = "";
  Name: string = "";
  Description: string = "";
  Icon: string = "";
}

/**
 * 对上层协议接口
 */
export class ProgramInfo4vue {
  public Key: string = ""; //应用程序名称
  public File: string = ""; //对应的下载文件名
  public Status: DownloadStatus = DownloadStatus.Undownload;
  public Progress: number = 0.0; //下载百分比 0 - 100
  // public dependencies: ProgramInfo[] = [] as ProgramInfo[]; //当前程序的依赖列表，所有依赖都平铺在当前一个层级，便于遍历
}

/*
export interface TaskConfig {
  [key: string]: DownloadTask;
}
*/

//{Key:'appname', action:'stop/continue/delete'}
export interface DownloadAction {
  Action: ActionEnum;
  AppKey: string;
  Url?: string;
  FileKey?: string;
  FileName?: string;
}
export interface SysInfo {
  ContainerStatus: ContainerStatus;
  WineVersion: string;
}
export interface depInfo {
  Key: string;
  Level: number;
  licenseUrl: string;
  HardDep?: string[];
  Version: string;
}

export interface depInfoLicense extends depInfo {
  Checked: boolean;
  Disabled: boolean;
  Status: DepStatus;
  HardDep: string[];
}
//const homeDir = os.homedir();
//const homedir = process.env.HOME!;

//let WorkDirConfig = new WorkDirCfg()

//export let WorkDirConfig = new WorkDirCfg();
