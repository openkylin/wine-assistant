#!/usr/bin/bash
#!/bin/sh

ENTRY=`zenity --width=300 --entry --title="安装程序需要sudo权限" --window-icon=./wine-assistant.png --text="请输入密码:" --hide-text --cancel-label="取消" --ok-label="确认"`

case $? in
  0)
	 	echo "`echo $ENTRY | cut -d'|' -f2`"
	;;
 	1)
		echo "";;
  -1)
		echo "";;
esac