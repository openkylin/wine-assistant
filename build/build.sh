#!/bin/bash
SCPATH="$(realpath $(dirname $0))"
DEBSPATH="${SCPATH}/kylin-wine-bottle/wine-assistant"
TAGPATH="${SCPATH}/debuild-script/wine-assistant"

if [ ! -d "$DEBSPATH" ]; then
	echo 获取打包脚本失败
	exit 1
fi

rm -rf "${TAGPATH}"

cd "${DEBSPATH}"
if [ $? != 0 ]; then exit 2;fi
python3 -m compileall .

pycpath=$( find . | egrep "__pycache__/")

for pych in $pycpath
do
  # echo $pych
	tmp1="${pych#*/}"
	tag_dir="${pych%/__pycache__*}"
	# echo $tmp1 $tag_dir
	tag_pyc=$(echo "${pych##*/}" | awk -F"." '{print $1"."$3}')
	mkdir -p "$TAGPATH/$tag_dir"
	cp  "$pych" "$TAGPATH/$tag_dir/$tag_pyc"
	echo "$pych" "$TAGPATH/$tag_dir/$tag_pyc"
done

cp "${DEBSPATH}/default.json" "${TAGPATH}"
cp  -rf "${DEBSPATH}/DebuildFile" "${TAGPATH}"
rm -rf ~/.wine-assistant/kylin-wine-bottle

mkdir -p  ~/.wine-assistant/
ln -s "${SCPATH}/debuild-script" ~/.wine-assistant/kylin-wine-bottle
