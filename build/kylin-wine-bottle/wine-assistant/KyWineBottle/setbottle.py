import os.path
import re
import time

from .common import *
from PIL import Image


class SetBottle:
    def __init__(self, bottle_info: dict):
        self.bottle_info = bottle_info
        self.xdg_path = f"{self.bottle_info['path']}/kylin-wine-menu"
        self.bottle = self.bottle_info["path"]
        self.build_info = None
        return

    def create(self):
        bottle = self.bottle
        # cmd = ". /opt/cxdepend/depend-env.sh && WINEARCH={} WINEPREFIX={} {} wineboot".format(self.bottle_info[
        # "arch"],
        cmd = "WINEARCH={} WINEPREFIX={} {} wineboot".format(self.bottle_info["arch"],
                                                             bottle, self.bottle_info["wine"])
        print(cmd)
        cmd_result = subprocess.run(cmd, shell=True, stderr=subprocess.PIPE)
        if cmd_result.stderr.decode('utf-8').find("Wine cannot find the") >= 0:
            raise Exception("创建容器失败：依赖不满足")

    def start(self, exe):
        bottle = self.bottle
        name = exe[0]
        param = ""
        if len(exe) > 1:
            param = " ".join(exe[1:]).replace("\-", "-")

        # cmd = f". /opt/cxdepend/depend-env.sh && XDG_CONFIG_HOME={self.xdg_path} XDG_DATA_HOME={self.xdg_path}"
        cmd = f"XDG_CONFIG_HOME={self.xdg_path} XDG_DATA_HOME={self.xdg_path}"
        cmd += " WINEARCH={} WINEPREFIX={} {} \"{}\" {}".format(self.bottle_info["arch"], bottle,
                                                                self.bottle_info["wine"],
                                                                name, param)
        print(cmd)
        process = subprocess.Popen(cmd, shell=True)
        if not process:
            raise Exception("运行 {} 失败".format(cmd))
        return process

    def start_sync(self, exe):
        process = self.start(exe)
        process.wait()

    def find_exe(self, exe):
        pattern = re.compile(exe, re.I)
        file = []
        for root, dirs, files in os.walk(self.bottle):
            for f in files:
                if pattern.search(f):
                    # print(os.path.join(root, f))
                    file.append(os.path.join(root, f))

        if not len(file):
            raise Exception("未找到程序：{}".format(exe))

        print("输入编号，选择以下应用运行：")
        count = 1
        for f in file:
            print("{} -> {}".format(count, f))
            count = count + 1

        num = input('Enter Number: ')
        if int(num.encode()) > count or int(num) < 1:
            raise Exception("{} 非法".format(num))

        return file[int(num) - 1]


class SetBuild:
    def __init__(self, build_info):
        self.build_info = build_info

    def build_deb(self, ):
        old_path = os.getcwd()
        build_path = "{}/KyWineBuild/{}-{}+{}/kylin-wine-{}-{}".format(old_path, self.build_info["Package"],
                                                                       self.build_info["Version"], time.time(),
                                                                       self.build_info["Package"],
                                                                       self.build_info["Version"].split("-")[0])

        if os.path.exists(build_path):
            raise Exception("打包目录已存在")

        os.makedirs(build_path)
        os.chdir(build_path)

        # 打包容器
        def ignore_func(src, names):
            dirs = [".update-timestamp", "dosdevices"]
            ignore = [name for name in names if name in dirs]
            return ignore

        if self.build_info["Env"]["WINEPREFIX"] is None:
            bottle_path = "{}/{}".format(BOTTLE_PATH, self.build_info["Package"])
        else:
            bottle_path = self.build_info["Env"]["WINEPREFIX"]
        shutil.copytree(bottle_path, self.build_info["Package"], symlinks=True, ignore=ignore_func)

        cmd = "mksquashfs {} {}.sqfs -comp xz".format(self.build_info["Package"], self.build_info["Package"])
        cmd_result = subprocess.run(cmd, shell=True, stderr=subprocess.PIPE)
        if cmd_result.returncode:
            raise Exception("打包失败")

        shutil.rmtree(self.build_info["Package"])

        # 复制 icon

        if self.build_info["Icon"] != "":
            icon_type = self.build_info["Icon"].split('.')[-1]
            if icon_type == "png" or icon_type == "PNG":
                with Image.open(self.build_info["Icon"]) as img:
                    # 获取图片的宽度和高度
                    width, height = img.size
            else:
                width = 48
                height = 48
            os.makedirs(f"icons/hicolor/{width}x{height}/apps/")
            icon = f"kylin-wine-{self.build_info['Package']}.{icon_type}"
            shutil.copyfile(self.build_info["Icon"], f"icons/hicolor/{width}x{height}/apps/{icon}")
        else:
            self.__get_icons()

        # 复制debian文件
        shutil.copytree(DEB_FILE + "/amd64", "debian")
        shutil.copyfile(DEB_FILE + "/kylin-wine-auth", "kylin-wine-auth")
        os.chmod("kylin-wine-auth", 0o755)

        self.__debian_info()

        cmd = "yes|debuild -sa -us -uc"
        cmd_result = subprocess.run(cmd, shell=True, stderr=subprocess.PIPE)

    def __debian_info(self):
        index = {
            "changelog": ["BOTTLE", "VERSION", "DEVEL_NAME", "DEVEL_MAIL", "TIME", "LOG"],
            "control": ["BOTTLE", "DEVEL_NAME", "DEVEL_MAIL", "DEPEND", "DESCRIPTION", "PACKAGEARCH"],
            "postinst": ["BOTTLE", "ARCH"],
            "postrm": ["BOTTLE", "ARCH"],
            "preinst": ["BOTTLE", "ARCH"],
            "prerm": ["BOTTLE", "ARCH"],
            "kylin-wine-ex": ["BOTTLE", "ARCH", "EXECPATH", "EXECNAME", "WINEBIN", "PARAM", "ENVIRONMENT"],
            "kylin-wine-ex.desktop": ["BOTTLE", "ICON", "NAME"],
            "install": ["BOTTLE"]
        }

        if os.path.exists("{}/drive_c/windows/syswow64".format(self.build_info["Env"]["WINEPREFIX"])):
            arch = "win64"
        else:
            arch = "win32"

        if self.build_info["Depends"] == "":
            self.build_info["Depends"] = WINE

        start_env = ""
        if self.build_info["Env"]:
            for key in self.build_info["Env"]:
                if key == "WINEPREFIX":
                    continue
                elif key == "WINEARCH":
                    arch = self.build_info["Env"][key]
                else:
                    start_env += f"{key}=\"{self.build_info['Env'][key]}\""

        log = ""
        if self.build_info["Log"] and len(self.build_info["Log"]) > 0:
            for item in self.build_info["Log"]:
                log += item + "\n    "

        package_acrh = "all"
        if "Architecture" in self.build_info:
            package_acrh = self.build_info["Architecture"]
        info = {
            "BOTTLE": self.build_info["Package"],
            "VERSION": self.build_info["Version"],
            "DEVEL_NAME": self.build_info["Maintainer"],
            "DEVEL_MAIL": self.build_info["EMail"],
            "TIME": time.strftime("%a, %d %b %Y %H:%M:%S"),
            "ARCH": arch,
            "DEPEND": self.build_info["Depends"],
            "LOG": log,
            "DESCRIPTION": self.build_info["Description"],
            "WINEBIN": WINE_PATH,
            "EXEC": self.build_info["Exec"],
            "EXECPATH": os.path.dirname(self.build_info["Exec"]),
            "EXECNAME": os.path.basename(self.build_info["Exec"]),
            "ICON": f"kylin-wine-{self.build_info['Package']}",
            "NAME": self.build_info["Name"],
            "PARAM": self.build_info["Param"],
            "ENVIRONMENT": start_env,
            "PACKAGEARCH": package_acrh
        }

        for k, v in index.items():
            file = f"debian/{k}"

            with open(file, "r+") as f:
                content = f.read()
                for field in v:
                    tmp = content.replace(f"%{field}%", info[field])
                    content = tmp
                f.seek(0)
                f.truncate()
                f.write(content)
            f.close()

        os.chmod(f"debian/postinst", 0o755)
        os.chmod(f"debian/postrm", 0o755)
        os.chmod(f"debian/preinst", 0o755)
        os.chmod(f"debian/prerm", 0o755)
        os.chmod("debian/kylin-wine-ex", 0o755)

        os.mkdir("start_script")
        shutil.move("debian/kylin-wine-ex", f"start_script/kylin-wine-{self.build_info['Package']}")
        shutil.move("debian/Disclaimers.txt", f"Disclaimers-{self.build_info['Package']}.txt")
        shutil.move("debian/kylin-wine-ex.desktop", f"kylin-wine-{self.build_info['Package']}.desktop")

        return
    
    def __get_icons(self):
        crossover_icon = "{}/{}/windata/cxmenu/icons".format(BOTTLE_PATH, self.build_info["Package"])
        if not os.path.exists(crossover_icon):
            if not os.path.exists(f"{BOTTLE_PATH}/{self.build_info['Package']}/icons"):
                print("未找到icons文件夹，请自行添加图标")
                return  
            shutil.copytree(f"{BOTTLE_PATH}/{self.build_info['Package']}/icons", "icons")
        else:
            shutil.copytree(crossover_icon, "icons")
        for dirpath, dirnames, filenames in os.walk("icons"):
            for filename in filenames:
                shutil.move(os.path.join(dirpath, filename), os.path.join(dirpath, "kylin-wine-" + filename))
