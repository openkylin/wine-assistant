import os
import sys
import argparse
import subprocess
import json
import hashlib
import json
import shutil

HOME = os.environ["HOME"]
WINE8_PATH = "/opt/kylin-wine/wine8"
WINE9_PATH = "/opt/kylin-wine/wine9"
if os.path.exists(WINE9_PATH):
    WINE_PATH = f"{WINE9_PATH}/bin/kylin-wine"
    WINE = "wine9"
elif os.path.exists(WINE8_PATH):
    WINE_PATH = f"{WINE8_PATH}/bin/kylin-wine"
    WINE = "wine8"
else:
    WINE_PATH = f"{WINE8_PATH}/bin/okylin-wine"
    WINE = "okylin-wine"
WINE_LIB = f"{WINE_PATH}/lib/wine"
WINE_SHARE = f"{WINE_PATH}/share"
BOTTLE_CONF = os.path.dirname(__file__) + "/../KyWineConfig"
DEB_FILE = os.path.dirname(__file__) + "/../DebuildFile"
BOTTLE_PATH = HOME + "/.okylin-wine"
DEFAULT_RULE_X32 = BOTTLE_CONF + "/default/default_win32.json"
DEFAULT_RULE_X64 = BOTTLE_CONF + "/default/default_win64.json"

