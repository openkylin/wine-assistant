#!/bin/python3
import os
import argparse
import KyWineBottle
import apt
import pprint
import shutil
import random
import string
import json
import hashlib

HOME = os.environ["HOME"]
WINE_WORK = HOME + "/.okylin-wine"
CONFIG_PATH = HOME + "/.wine-assistant/config"
CONFIG = CONFIG_PATH + "/winfixdll.json"
WINE8_PATH = "/opt/kylin-wine/wine8"
WINE9_PATH = "/opt/kylin-wine/wine9"
WINE_DLL_FIX = "drive_c/WinDllFix"
SCR_PATH = os.path.abspath(__file__)
SCR_DIR = os.path.dirname(SCR_PATH)


def parse_args():
    parser = argparse.ArgumentParser(description='设置容器工具')
    subparsers = parser.add_subparsers(dest="sub", required=True)

    par2 = subparsers.add_parser("create", help="创建容器")
    par2.add_argument('-n', '--name', type=str, required=False, help='容器名称，可以为字母与数字的集合')
    par2.add_argument('-a', '--arch', type=str, required=False, choices=["win32", "win64"],
                      help='指定容器架构,默认为win32')
    par2.add_argument('-w', '--wine', type=str, required=False, choices=["wine8", "wine9"],
                      help='指定wine版本')
    # -----------------------------------------
    par5 = subparsers.add_parser("run", help="启动Windows程序")
    par5.add_argument('-n', '--name', type=str, required=False, help='容器名称，可以为字母与数字的集合')
    par5.add_argument('-e', '--exe', nargs="+", required=False, help='Windows 应用地址与参数')
    par5.add_argument('-f', '--fuzzy', nargs="+", required=False, help='Windows 应用名(模糊匹配)')
    par5.add_argument('-w', '--wine', type=str, required=False, choices=["wine8", "wine9"],
                      help='指定wine版本')
    # -----------------------------------------
    par6 = subparsers.add_parser("debuild", help="生成打包目录， 包名默认为kylin-wine-{name}")
    par6.add_argument('-w', '--wine', type=str, required=False, choices=["wine8", "wine9"],
                      help='指定wine版本')
    par6.add_argument('-f', '--file', type=str, help='打包配置文件路径')
    # -----------------------------------------
    par7 = subparsers.add_parser("dll", help="设置dll")
    par7.add_argument('-n', '--name', required=False, type=str, help='容器名称，可以为字母与数字的集合')
    par7.add_argument('-w', '--wine', type=str, required=False, choices=["wine8", "wine9"],
                      help='指定wine版本')

    par7_g1 = par7.add_mutually_exclusive_group()
    par7_g1.add_argument('-l', '--list', action='store_true', help='展示支持修改的列表')
    par7_g1.add_argument('-d', '--dlls', nargs="+", help='需要修改的dll名称可以输入多个')

    par7_g2 = par7.add_mutually_exclusive_group()
    par7_g2.add_argument('-t', '--type', type=str,
                      choices=["n", "b", "s", "nb", "bn"],
                      help='需要修改dll的加载方式,可选项为：n: 原生、b: 内建、s: 停用、nb:原生先于内建、\
                            bn:内建先于原生')

    return parser.parse_args()


def get_bottle_info(iscreate=False):
    arg = parse_args()

    if arg.wine is None:
        if os.path.exists(WINE9_PATH):
            wine = f"{WINE9_PATH}/bin/kylin-wine"
        elif os.path.exists(WINE8_PATH):
            wine = f"{WINE8_PATH}/bin/kylin-wine"
        else:
            raise Exception("未找到kylin wine")
    else:
        wine = f"{arg.wine}/bin/kylin-wine"
        if not os.path.exists(wine):
            raise Exception(f"未找到kylin wine: {wine}")

    if arg.name is None:
        name = "default"
    else:
        name = arg.name

    bottle = WINE_WORK + "/" + name

    if not iscreate:
        if not os.path.exists(bottle):
            raise Exception("容器不存在: {}".format(bottle))
        if not os.path.exists(bottle + "/drive_c/windows/system32"):
            raise Exception("容器异常，未发现 {}/drive_c/windows/system32 目录".format(bottle))
        if os.path.exists(bottle + "/drive_c/windows/syswow64"):
            arch = "win64"
        else:
            arch = "win32"
    else:
        if arg.arch is None:
            arch = "win32"
        else:
            arch = arg.arch

    return {
        "arch": arch,
        "name": name,
        "path": bottle,
        "wine": wine
    }


def create_bottle():
    if not os.path.exists(WINE_WORK):
        os.makedirs(WINE_WORK)

    bottle_info = get_bottle_info(iscreate=True)
    if os.path.exists(bottle_info["path"]):
        raise Exception("容器已存在：{}".format(bottle_info["path"]))
    setbot = KyWineBottle.SetBottle(bottle_info)
    setbot.create()


def run_exe():
    arg = parse_args()
    bottle_info = get_bottle_info()
    bot = KyWineBottle.SetBottle(bottle_info)

    if arg.exe is not None:
        bot.start(arg.exe)
    elif arg.fuzzy is not None:
        print(arg.fuzzy)
        exe = bot.find_exe(arg.fuzzy[0])
        arg.fuzzy[0] = exe
        bot.start(arg.fuzzy)
    else:
        raise Exception("未指定应用名")
    return


def set_dll():
    arg = parse_args()
    bottle_info = get_bottle_info()

    if arg.list:
        data = dll_list(bottle_info["wine"])
        pprint.pprint(data)
        return

    if arg.dlls and arg.type:
        dll_replace(bottle_info)


def dll_replace(bottle_info):
    dll_path = os.path.join(bottle_info["path"], WINE_DLL_FIX)
    save_path = os.path.join(bottle_info["path"], "drive_c/windows/nativedlls")
    reg = "\
REGEDIT4\n\n\
[HKEY_CURRENT_USER\Software\Wine\DllOverrides]\n"
    data = dll_list(bottle_info["wine"])
    arg = parse_args()
    replace = False
    if arg.type == "n":
        dll_type = "native"
        replace = True
    elif arg.type == "b":
        dll_type = "builtin"
    elif arg.type == "nb":
        dll_type = "native,builtin"
        replace = True
    elif arg.type == "bn":
        dll_type = "builtin,native"
    else:
        exit(1)

    if replace:
        if os.path.exists(os.path.join(bottle_info["path"], WINE_DLL_FIX)):
            bottle_dll = __get_fixdll(bottle_info)
            if bottle_dll != data:
                print(bottle_dll)
                print(data)
                install_fixdll(bottle_info)
        else:
            install_fixdll(bottle_info)

    for d in arg.dlls:
        if replace:
            key = __mv_dll(data, d, dll_path, save_path)
        name = d.rsplit('.', 1)
        name.append("dll")
        if name[1] == "dll":
             reg += '"{}"="{}"\n'.format(name[0], dll_type)
        else:
            reg += '"{}"="{}"\n'.format(name[0] + "." + name[1], dll_type)

    random_string = ''.join(random.choices(string.ascii_letters + string.digits, k=8))
    reg_file = "/tmp/tmp" + random_string + ".reg"
    print(reg)
    with open(reg_file, "w") as f:
        f.write(reg)
    f.close()
    bot = KyWineBottle.SetBottle(bottle_info)
    bot.start_sync(["reg", "import", reg_file])
    os.remove(reg_file)


def dll_list(wine):
    md5_fix_dll = hash_file(SCR_DIR + "/../WinDllFix.exe")
    if os.path.exists(CONFIG):
        with open(CONFIG, 'r') as f:
            # 使用json.load()方法解析文件
            old_data = json.load(f)
        f.close()
        if "WinDllFix.exe" in old_data and old_data["WinDllFix.exe"] == md5_fix_dll:
            return old_data

    bottle_info = {
        "arch": "win64",
        "name": "WineGetDllFix",
        "path": HOME + "/.WineGetDllFix",
        "wine": wine,
    }
    install_fixdll(bottle_info)
    new_data = __get_fixdll(bottle_info)

    if not os.path.exists(CONFIG_PATH):
        os.makedirs(CONFIG_PATH)
    with open(CONFIG, 'w') as f:
        json.dump(new_data, f)
    f.close()
    return new_data


def hash_file(file):
    md5_hash = hashlib.md5()
    with open(file, "rb") as f:
        while chunk := f.read(8192):
            md5_hash.update(chunk)
        md5_hex = md5_hash.hexdigest()
    f.close()
    return md5_hex


def install_fixdll(bottle_info):
    dll_path = os.path.join(bottle_info["path"], WINE_DLL_FIX)
    bot = KyWineBottle.SetBottle(bottle_info)

    print("开始安装软件:WinDllFix.exe")
    print([SCR_DIR + "/../WinDllFix.exe", "/VERYSILENT"])
    bot.start_sync([SCR_DIR + "/../WinDllFix.exe", "/VERYSILENT"])

    if not os.path.exists(dll_path):
        raise Exception("安装软件 WinDllFix.exe 失败")


def __get_fixdll(bottle_info):
    dll_path = os.path.join(bottle_info["path"], WINE_DLL_FIX)

    all_files = {}
    for root, dirs, files in os.walk(os.path.join(dll_path, "32")):
        for file in files:
            name = file.rsplit('.', 1)
            name.append("NoSuffix")
            if name[1] in all_files:
                all_files[name[1]][name[0]] = {"32": hash_file(os.path.join(root, file))}
            else:
                all_files[name[1]] = {name[0]: {"32": hash_file(os.path.join(root, file))}}
    for root, dirs, files in os.walk(os.path.join(dll_path, "64")):
        for file in files:
            name = file.rsplit('.', 1)
            name.append("NoSuffix")
            if name[1] in all_files:
                if name[0] in all_files[name[1]]:
                    all_files[name[1]][name[0]]["64"] = hash_file(os.path.join(root, file))
                else:
                    all_files[name[1]][name[0]] = {"64": hash_file(os.path.join(root, file))}
            else:
                all_files[name[1]] = {name[0]: {"64": hash_file(os.path.join(root, file))}}
    # pprint.pprint(all_files)
    all_files["WinDllFix.exe"] = hash_file(SCR_DIR + "/../WinDllFix.exe")
    return all_files


def __mv_dll(dlls: dict, dll: str, dll_path: str, save_path):
    arg = parse_args()
    name = dll.rsplit('.', 1)
    name.append("dll")

    save_path_32 = os.path.join(save_path, "32")
    save_path_64 = os.path.join(save_path, "64")
    if not os.path.exists(save_path_32):
        os.makedirs(save_path_32)
    if not os.path.exists(save_path_64):
        os.makedirs(save_path_64)

    file = []
    if name[1] in dlls:
        if name[0] in dlls[name[1]]:
            info: str = dlls[name[1]][name[0]]
            if "32" in info:
                file.append(os.path.join(dll_path, "32", name[0] + "." + name[1]))
                shutil.copyfile(file[-1], os.path.join(save_path_32, name[0] + "." + name[1]))
            if "64" in info:
                file.append(os.path.join(dll_path, "64", name[0] + "." + name[1]))
                shutil.copyfile(file[-1], os.path.join(save_path_64, name[0] + "." + name[1]))
    if file == []:
        if arg.type == "n" or arg.type == "nb":
            print("INFO: {} 未在 {} 中找到".format(dll, dll_path))
    if name[1] == "dll":
        return name[0]
    else:
        return name[0] + "." + name[1]


def build_deb():
    cache = apt.Cache()
    debs = ["dh-make", "devscripts", "build-essential", "squashfs-tools"]
    for d in debs:
        if d not in cache:
            print("工具打包需要依赖：dh-make devscripts build-essential squashfs-tools")
            return
        if not cache[d].is_installed:
            print("工具打包需要依赖：dh-make devscripts build-essential squashfs-tools")
            return
    arg = parse_args()

    if arg.file:
        if not os.path.exists(arg.file):
            raise Exception("配置文件不存在")
    else:
        raise Exception("错误参数")

    with open(arg.file, 'r') as f:
        # 使用json.load()方法解析文件
        data = json.load(f)
    f.close()

    pprint.pprint(data)
    if "buildPath" in data:
        os.chdir(data["buildPath"])
    else:
        build_path = os.path.join(HOME, "WineAssistantBuild")
        if not os.path.exists(build_path):
            os.makedirs(build_path)
        os.chdir(build_path)
    bot = KyWineBottle.SetBuild(data)
    bot.build_deb()


def main():
    arg = parse_args()
    if arg.sub == "create":
        create_bottle()
    if arg.sub == "run":
        run_exe()
    elif arg.sub == "dll":
        set_dll()
    elif arg.sub == "debuild":
        try:
            build_deb()
        except Exception as e:  # 捕获所有从 Exception 继承的异常  
            print(f"捕获到异常: {e}")  
        input_line = input("请输入回车结束: ");
    else:
        raise Exception("未知参数")


if __name__ == '__main__':
    main()
