/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { useI18n } from "vue-i18n";
export class Verify {
  private info: number[] = [];
  private errorInfo: string[];
  constructor() {
    const { t } = useI18n();
    this.errorInfo = [
      "",
      t("bottle.other.noEmpty"),
      t("bottle.other.numLowercase"),
      t("bottle.other.selectFolder"),
      t("bottle.other.pathNotExist"),
      t("bottle.other.emailIllegal"),
      t("bottle.other.startWithNumbers"),
    ];
  }
  public getErrorInfo() {
    return this.errorInfo;
  }
  async verify(config: [string, any][]): Promise<number[]> {
    this.info = [];
    for (let i = 0; i < config.length; i++) {
      let [key, value] = config[i];
      if (key === "Name") {
        this.info.push(this.verNULL(value));
      } else if (key === "Depends") {
        this.info.push(this.verNULL(value));
      } else if (key === "Maintainer") {
        this.info.push(this.verNULL(value));
      } else if (key === "Description") {
        this.info.push(this.verNULL(value));
      } else if (key === "Exec") {
        this.info.push(await this.verPath(value));
      } else if (key === "Icon") {
        this.info.push(await this.verPath(value));
      } else if (key === "logfile") {
        this.info.push(await this.verPath(value));
      } else if (key === "Folder") {
        this.info.push(this.verFolder(value));
      } else if (key === "Package") {
        this.info.push(this.verPackage(value));
      } else if (key === "EMail") {
        this.info.push(this.verEmail(value));
      } else if (key === "Log") {
        this.info.push(this.verNULL(value));
      } else if (key === "Version") {
        this.info.push(this.verVersion(value));
      } else if (key === "Architecture") {
        this.info.push(this.verNULL(value));
      } else {
        // 未校验的字段返回-1
        this.info.push(-1);
      }
    }
    return this.info;
  }

  async verifys(config: [string, any][]): Promise<number[]> {
    this.info = [];
    for (let i = 0; i < config.length; i++) {
      let [key, value] = config[i];
      switch (key) {
        case "null":
          this.info.push(this.verNULL(value));
          break;
        case "email":
          this.info.push(this.verEmail(value));
          break;
        case "path":
          this.info.push(await this.verPath(value));
          break;
        case "folder":
          this.info.push(this.verFolder(value));
          break;
        case "package":
          this.info.push(this.verPackage(value));
          break;
        case "version":
          this.info.push(this.verVersion(value));
          break;
        case "dependencies":
          this.info.push(this.verDependency(value));
          break;
        default:
          this.info.push(-1);
          break;
      }
    }
    return this.info;
  }

  public verifyAllNotNull(config: [string, any][]): Promise<number[]> {
    return new Promise<number[]>((res, _rej) => {
      this.info = [];
      for (let i = 0; i < config.length; i++) {
        let [key, value] = config[i];
        if (key === "Dependencies") {
          if (value.length < 1) {
            this.info.push(1);
          } else {
            this.info.push(0);
          }
        } else {
          this.info.push(this.verNULL(value));
        }
      }
      res(this.info);
    });
  }

  //校验不能为空
  private verNULL(str: string): number {
    if (!str) {
      return 1;
    } else {
      return 0;
    }
  }
  private verEmail(Email: string): number {
    if (!Email) {
      return 1;
    } else if (
      !/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(Email)
    ) {
      return 5;
    } else {
      return 0;
    }
  }
  // Exec  Icon
  private verPath(path: string): Promise<number> {
    return new Promise((res, _rej) => {
      if (!path) {
        res(1);
      } else {
        window.assistantApi.fileType(path, (fileType) => {
          if (fileType === "file") {
            res(0);
          } else {
            res(4);
          }
        });
      }
    });
  }
  private verFolder(Folder: []): number {
    if (Folder.length < 1) {
      return 3;
    } else {
      return 0;
    }
  }
  private verPackage(Package: string): number {
    if (!Package) {
      return 1;
    } else if (!/^[a-z0-9\-]+$/.test(Package)) {
      return 2;
    } else {
      return 0;
    }
  }
  private verVersion(Version: string): number {
    if (!Version) {
      return 1;
    } else if (!/^[0-9][0-9a-z+.~\-]*$/.test(Version)) {
      return 6;
    } else {
      return 0;
    }
  }
  private verDependency(str): number {
    if (str.length < 1) {
      return 1;
    } else {
      return 0;
    }
  }
}
