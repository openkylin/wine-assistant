/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { Verify } from "./verify";
export class Utils {
  private errorInfo: string[];
  private verify = new Verify();
  constructor() {
    this.errorInfo = this.verify.getErrorInfo();
  }
  public stepTitleColor = (
    stepStatus,
    step: number,
    first: string,
    second: string,
    third: string
  ) => {
    let baseClass = "";
    let afterClass = ""; //finish状态时后面的horizontal颜色为橙色
    if (step === 1) {
      if (stepStatus["active"] === first || stepStatus[first] === "process") {
        baseClass = "process-title-css";
      } else if (stepStatus[first] === "finish") {
        baseClass = "finish-title-css";
      }
    } else if (step === 2) {
      if (stepStatus["active"] === second || stepStatus[second] === "process") {
        baseClass = "process-title-css";
      } else if (stepStatus[second] === "finish") {
        baseClass = "finish-title-css";
      } else if (stepStatus[second] === "wait") {
        baseClass = "wait-title-css";
      }
    } else if (step === 3) {
      if (stepStatus["active"] === third || stepStatus[third] === "process") {
        baseClass = "process-title-css";
      } else if (stepStatus[third] === "wait") {
        baseClass = "wait-title-css";
      }
    }
    if (stepStatus[first] === "finish" && step === 1) {
      afterClass = "horizon-title-css";
    } else if (stepStatus[second] === "finish" && step === 2) {
      afterClass = "horizon-title-css";
    }
    return `${baseClass} ${afterClass}`;
  };

  public envTagChange(item, newTags) {
    let flag = "0";
    if (item.hasOwnProperty("Env")) {
      item.Env = {};
      flag = "1";
    } else if (item.hasOwnProperty("wineenv")) {
      item.wineenv = {};
      flag = "2";
    }
    newTags.forEach((tag) => {
      let index = tag.indexOf("=");
      let key, val;
      if (index !== -1) {
        key = tag.substring(0, index);
        val = tag.substring(index + 1);
      } else {
        key = "";
        val = "";
      }
      val = val.trim();
      if (val.startsWith('"') && val.endsWith('"')) {
        val = val.slice(1, -1);
      } else if (val.startsWith("'") && val.endsWith("'")) {
        val = val.slice(1, -1);
      }
      if (key && flag === "1") {
        item.Env[key] = val;
      } else if (key && flag === "2") {
        item.wineenv[key] = val;
      }
    });
  }

  async allocateDatas(info: {}, validate) {
    let ver: [string, any][] = [];
    let temp = "";
    let keys: string[] = [];
    for (let key in info) {
      if (validate.hasOwnProperty(key)) {
        ver.push([validate[key]["verify"], info[key]]);
        keys.push(key);
      }
    }
    let conf = await this.verify.verifys(ver);
    for (let i = 0; i < conf.length; i++) {
      let index = conf[i];
      if (index === -1) {
        continue;
      }
      temp = this.errorInfo[conf[i]];
      validate[keys[i]].help = temp;
      if (temp === "") {
        validate[keys[i]].status = "validating";
      } else {
        validate[keys[i]].status = "error";
      }
    }
  }
}
