/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
/**
 * {title:'Name', dataIndex:"name"},
  {title:'Category', dataIndex:"category"},
  {title:'Description', dataIndex:"descr",ellipsis: true,tooltip: {position: 'left'}, width:150 },
  {title:'Grade', dataIndex:"grade"},
  {title:'Arch', dataIndex:"arch"},
  {title:'LinuxArch', dataIndex:"linuxArch"},
  {title:'Optional', slotName:'optional'}
 */
export default {
  errors: {
    depend: {
      installFail: "Installation failed",
      notExist: "Dependency not exist",
      parseFail: "Parse depends failed",
    },
    program: {
      launchFail: "Launch fail",
    },
  },
  menu: {
    programs: "Programs",
    dependencies: "Dependencies",
    components: "Components",
    local: "Local install",
    programsList: "All Software",
    programsAdmin: "Software Manager",
    bottle: "Bottle Manager",
  },
  programs: {
    header: "Program Manager",
    body: {
      table: {
        title: {
          name: "Name",
          category: "category",
          description: "Description",
          grade: "Grade",
          arch: "Arch",
          linuxArch: "LinuxArch",
          optional: "Optional",
        },
        row: {
          installBtn: "Install",
          detailBtn: "Detail",
          downloadBtn: "Download",
          installingBtn: "Installing...",
          execBtn: "Launch",
          uninstallBtn: "Uninstall",
        },
        filter: {
          confirmBtn: "Confirm",
          resetBtn: "Reset",
          placeholder: "Please input name",
        },
      },
      modal: {
        detailDialogOK: "Download & Install",
        detailDialogCancel: "Cancel",
        detailDialogTitle: "Installation Prompt",
        detailDialogContent:
          "About to visit a third-party software store page, please download the software from the store page. Once the download is complete, Wine Assistant will automatically proceed with the installation process. <br><p style='font-style:italic'><b>Reminder:</b> When downloading software from a third-party download page, please download the application through the \"regular download\" option.",
        programTitle: "Installing program...",
        analyzeDependTitle: "Analyzing dependencies...",
        dependTitle: "Installing dependencies...",
        installed: "Installed",
        installFail: "Install fail: ",
        uninstall: "Uninstall",
        initing: "Wine runtime environment is initializing...",
        deleted:
          "The Wine runtime environment has been manually deleted. Please restart the Wine assistant to attempt fixing this issue.",
        failed:
          "The initialization of the Wine runtime environment failed. You can seek help from the openKylin community",
      },
    },
  },
  dependencies: {
    header: "Dependency Manager",
    body: {
      table: {
        title: {
          name: "Name",
          description: "Description",
          category: "category",
          optional: "Optional",
        },
        row: {
          installBtn: "Install",
        },
      },
    },
  },
  components: {
    header: "Component Manager",
    runtimeEnvManager: "Runtime environment management",
    displayWineInfo: "Display Wine related information...",
  },
  programsAdmin: {
    header: "Software Manager",
    button: {
      start: "Start",
      install: "Install",
      uninstall: "Uninstall",
    },
  },
  programsList: {
    header: "All Software",
    modal: {
      initing: "Wine runtime environment is initializing...",
      deleted:
        "The Wine runtime environment has been manually deleted. Please restart the Wine assistant to attempt fixing this issue.",
      failed:
        "The initialization of the Wine runtime environment failed. You can seek help from the openKylin community",
      analyzeDependTitle: "Analyzing dependencies...",
      dependTitle: "Installing dependencies...",
      chooseInstallPackage:
        "Choose the installation package acquistion method:",
      dialogContent:
        "About to visit a third-party software store page, please download the software from the store page. Once the download is complete, Wine Assistant will automatically proceed with the installation process.",
      networkDownContent:
        "The latest software version of Wine Assistant will be downloaded and installed on the network. When the download is completed, Wine Assistant will automatically carry out the subsequent installation process.",
      thirdSoftStoreDown:
        "Go to the third-party software store to download and install the software package",
      networkDownWine:
        "Download and install the latest software version of Wine assistant adaptation from the network",
      localInstallSoft:
        "Install the software package by selecting an existing one on your local Windows system",
      localInstallFile: "Local installation file",
      networkDownload: "Network download",
      thirdSoftStore: "Third party software store",
      downloadComplete: "Download completed",
      noClose:
        "Please don't close Wine Assistant while the software is being downloaded and installed!!!",
      installFail: "Install fail:  ",
      installed: "Installed",
      uninstall: "Uninstall",
      waitInstallSoft: "The software is being installed,please wait...",
      noSearchSoft: "No target application found",
      searchSuccess: "Search successful",
    },
    button: {
      detailDialogOK: "Download & Install",
      detailDialogCancel: "Cancel",
      install: "Install",
      downloadBtn: "Download",
      installing: "Installing",
      execBtn: "Startup",
    },
    input: {
      softName: "Please enter the software name",
      filePath: "File path",
      softInstallPackagePath:
        "Please enter the path to the softmax installation package",
    },
  },
  bottle: {
    header: "Bottle Manager",
    title: {
      baseInfo: "Basic Information",
      runSoft: "Run Software",
      runProgram: "Run Program",
      shortCut: "Create Shortcut",
      bottleOperation: "Bottle Operation",
      bottleConfig: "Bottle Config",
      bottleBackup: "Bottle Backup",
      createBackup: "Create Backup File",
      importBackup: "Import Backup File",
      deriveDeb: "Export DEB Package",
      setDll: "Set WinDllFix",
      winecfg: "Wine Settings",
      driveC: "Drive C",
      register: "Registry",
      Network: "Network Settings",
      componentInstall: "Component Installation",
      installComponents: "Install Components",
      wineKillAll: "Close All Software",
      insDepending: "The following components are being installed",
      startItem: "Startup Item",
      logInfo: "Log Information",
      controlInfo: "Control Information",
      exportSoftConf: "Export Software Configuration",
      baseInfoE: "Basic Information",
      installPackE: "Installation Package Information",
      startParamE: "Startup Parameters",
      chooseExportPath: "Select the export path",
    },
    button: {
      confirm: "Confirm",
      confirmBtn: "Confirm",
      open: "Open",
      close: "Close",
      install: "Install",
      cancel: "Cancel",
      previous: "Previous Step",
      next: "Next Step",
      addInstallPack: "Add the installation package",
    },
    body: {
      wineVersion: "Wine version:",
      bottleVersion: "Bottle version:",
      soft: "Software:",
      component: "Component:",
      programPath: "Program path:",
      startParam: "Startup parameters:",
      logChannel: "Log channel:",
      logFile: "Log file:",
      env: "Environment variable:",
      backupPath: "Backup path:",
      backupFile: "Backup file:",
      type: "Type:",
      name: "Name:",
      storagePath: "Storage path:",
      dllList: "Dll list:",
      icon: "Icon:",
      log: "Log:",
      version: "Version:",
      description: "Description:",
      email: "EMail:",
      maintainer: "Maintainer:",
      depends: "Depends:",
      architecture: "Architecture:",
      package: "Package:",
      softName: "Name:",
      softArch: "Architecture:",
      grade: "Grade:",
      category: "Category:",
      detailUrl: "DetailUrl:",
      installPackage: "Installation package:",
      action: "Action:",
      fileType: "File type:",
      protable: "Protable:",
      url: "Url:",
      script: "Script:",
      startProgram: "Start program:",
      installPath: "Installation path:",
      exportPath: "Export path:",
      startName: "Startup file name",
      installName: "Installation file name",
      startPath: "Startup file path",
      installFilePath: "Installation file path",
    },
    input: {
      programPath: "Please enter the program path",
      startParam: "Please enter the startup parameters",
      logChannel: "Please select the log channel",
      logFile: "Please enter the log file",
      env: "Please enter environment variables",
      openLocalFile: "Open local file",
      importBackupFile: "Please select the backup file to import",
      createBackupPath: "Please select the backup file path for export",
      name: "Please enter a name",
      desktop: "Desktop",
      startMenu: "Start menu",
      native: "Native",
      builtIn: "Built-in",
      nativeBeforeBuiltIn: "Native before Built-in",
      builtInBeforeNative: "Built-in before Native",
      component: "Please select the component to be set ...",
      componentType: "Please select the type of component to be set ...",
      choose: "Please select ...",
      icon: "Please enter the icon",
      log: "Please enter the log details",
      version: "Please enter the version",
      description: "Please enter the software description",
      email: "Please enter the email address of the person in charge",
      maintainer: "Please enter the person in charge",
      depends: "Please enter the depends",
      package: "Please enter the package name",
      softName: "Please select or enter the name of the software",
      category: "Please select or enter the category",
      dependency: "Please select or enter the dependencies",
      iconUrl: "Please enter the URL of the icon",
      detailUrl:
        "Please enter the official website URL of the software's homepage",
      fileName: "Please enter the ",
      url: "Please enter the download link",
      script: "Please enter the execution commands",
      startProgram: "Please enter the startup program",
      installPath: "Please enter the installation path",
      exportPath: "Please enter the file export path",
    },
    output: {
      createBackupSuccess: "Backup exported successfully",
      createBackupFail: "Backup export failed",
      setDllSuccess: "WinDllFix setting was successful",
      setDllFail: "WinDllFix setting failed",
      importBackupFail: "Backup import failed",
      softInstalling: "There is software being installed. Please wait...",
      createEnvFail: "Environment creation failed",
      startDrive:
        "Start packaging. The packaging process will take some time. Please be patient and wait...",
      componentInstalled: "The component has been installed in the bottle.",
      installed: "Installation completed",
      createShortcutSuccess: "Shortcut created successfully",
      createShortcutFail: "Shortcut creation failed",
      exportSoftConfSuccess:
        "The configuration files have been exported successfully",
      exportSoftConfFail: "Configuration file export failed",
      installPackNotEmpty: "Installation package information cannot be empty",
    },
    category: {
      social: "Social",
      economic: "Economic",
      network: "Network",
      educational: "Educational",
      office: "Office",
      audiovisual: "Audiovisual",
      image: "Image",
      game: "Game",
      system: "System",
      develtop: "Develtop",
      other: "Other",
    },
    problem: {
      collapse: "collapse problem",
      fileAccess: "file access problem",
      font: "font problem",
      gameController: "game controller problem",
      graphics: "graphics problem",
      keyboardInput: "keyboard input problem",
      mouseInput: "mouse input problem",
      networkConnect: "network connection problem",
      print: "printing problem",
      sound: "sound problem",
      window: "window problem",
    },
    other: {
      installComponents: "When installing the component ",
      insDepends:
        ", the following content needs to be installed. Do you agree to the user agreement and install the component?",
      userAgreement: "component user agreement:",
      none: "none",
      installFail: "Install fail: ",
      installed: "Installed",
      uninstall: "Uninstall",
      noEmpty: "Cannot be empty",
      numLowercase:
        "Can only contain numbers, lowercase letters and '-',and cannot contain spaces and underscores",
      selectFolder: "Select at least one folder",
      pathNotExist: "The path does not exist or is not a file",
      emailIllegal: "The email address is illegal",
      startWithNumbers:
        "Can only contain numbers, lowercase letters,+,.,-,~ and other symbols, and can only start with numbers",
      importBackup:
        "Backup file is being imported. Please wait. After the import is successful,Wine Assistant will restart.",
      createBackup: "Backup file is being generated, please wait...",
      loadBottleInfo: "Loading bottle information, please wait...",
      chooseComponent: "No component to be set has been selected",
      chooseType: "No type to be set has been selected",
      folderNotExist: "The path is not a folder or does not exist",
      chooseSquashfs: "The selected path does not seem to be a squashfs file",
    },
  },
  webPopup: {
    modal: {
      softInstallClose:
        "The software has not been downloaded and installed yet. If you exit now, you need to reinstall it next time. Are you sure to exit Wine Assistant?",
      dependInstallClose:
        "The dependent components have not been downloaded and installed yet. If you exit now, you need to reinstall it next time. Are you sure to exit Wine Assistant?",
      importBackupClose:
        "Exiting the Wine assistant will immediately stop the import process of the current backup file. Are you sure you want to exit?",
      createBackupClose:
        "Exiting the Wine assistant will immediately stop the export process of the current backup file. Are you sure you want to exit?",
      noClose:
        "Please don't close Wine Assistant while the software is being downloaded and installed!!!",
      tip: "Tip",
      detect: "It is detected that the link you clicked may not be ",
      normalDownload: "Normal Download",
      buttonLink:
        " button link. Entering other pages may cause software anomalies. Do you want to continue to visit?",
      cancel: "Cancel",
      agree: "Agree",
      confirm: "Confirm",
      newLink: "New Link:",
      oldLink: "Old Link:",
      restoreFactorySetting:
        "Restore factory settings, all installed software packages will be uninstalled and deleted, and Wine Assistant will be restarted!",
      detectLink: "It is detected that the download link you clicked is not '",
      selectDownloadLink:
        "' or the link may have been updated. Please select the download link and install it with Wine Assistant.",
      downloadOrNot: "Do you want to download '",
      installWithWine: "' and install it with Wine Assistant?",
      modifySystemParameter:
        "The value of system parameter fs.inotify.max_user_instances is too small, which may affect the normal use of Wine Assistant. It is suggested to modify the value of fs.inotify.max_user_instances to 1024 or above.",
    },
  },
  license: {
    title: "tip",
    ensure: "To ensure the normal operation of ",
    ensure2: ", please agree and check the agreement of ",
    ensure3: " component",
    agreeAndInstall: "The installed program depends on the ",
    agreeAndInstall2:
      " component. The following is the user agreement of this component. Do you agree and install it?",
    agreeAll: "Agree all",
    updatable: "[updatable]",
    readAndAccept: "Read and accept the user agreement of the ",
    readAndAccept2: " component:",
    none: "none",
    cancel: "Cancel",
    agree: "Agree",
    check: "Please check at least one item!",
  },
};
