/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { createApp } from "vue";
import App from "./App.vue";
import ArcoVue from "@arco-design/web-vue";
import ArcoVueIcon from "@arco-design/web-vue/es/icon";

import "@arco-design/web-vue/dist/arco.css";
import router from "./routes.ts";
import i18n from "./i18n.ts";
import { Notification } from "@arco-design/web-vue";
const app = createApp(App);
app.use(ArcoVue);
app.use(ArcoVueIcon);
app.use(router);
app.use(i18n);
app.config.globalProperties.$notification = Notification;
app.mount("#app");
