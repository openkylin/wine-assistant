/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 *
 */
import { createI18n } from "vue-i18n";
import zh from "./langs/zh";
import en from "./langs/en";

const messages = {
  en,
  zh,
};

const language = (navigator.language || "zh").toLocaleLowerCase();

const i18n = createI18n({
  locale: localStorage.getItem("lang") || language.split("-")[0] || "zh",
  fallbackLocale: "zh",
  legacy: false,
  messages,
});
export default i18n;
