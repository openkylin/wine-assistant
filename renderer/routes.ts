/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { createRouter, createWebHashHistory } from "vue-router";
// import Programs from "./components/Programs.vue";
import License from "./components/License.vue";
import Components from "./components/Components.vue";
import ProgramsList from "./components/ProgramsList.vue";
import ProgramsAdmin from "./components/ProgramsAdmin.vue";
import Container from "./components/Bottle.vue";
import webPopup from "./components/webPopup.vue";
const routes = [
  { path: "/", redirect: "/programsList" },
  { path: "/components", component: Components },
  // { path: "/programs", component: Programs },
  { path: "/license", component: License },
  { path: "/programsList", component: ProgramsList },
  { path: "/programsAdmin", component: ProgramsAdmin },
  { path: "/container", component: Container },
  { path: "/webPopup", component: webPopup },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export default router;
