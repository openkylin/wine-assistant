/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
//import { ProgramCategory } from "../main/commons/define";

//import { ActionEnum } from "../main/commons/define";

type ipcCallback = (event: Electron.IpcRendererEvent, ...args: any[]) => void;

interface Window {
  assistantApi: {
    getSoftInfo: (callback?: viewCallback) => void;
    isLicense: () => boolean;
    getPrograms: (callback?: viewCallback) => void;
    getDependencies: (param: any, callback?: viewCallback) => void;

    installDependencies: (
      keys: {}[],
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    recursiveProgramDeps: (
      category: string,
      key: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    getProgramDepsLicense: (
      category: string,
      key: string,
      bottle?: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    install: (
      category: ProgramCategory,
      key: string,
      RealUrl?: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    openDetail: (
      param: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    openLicensePage: (
      param: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    getLicenseUrl: (
      param: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;

    uninstall: (
      key: string,
      name: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    opDownload: (
      appKey: string,
      fileKey: string,
      op: ActionEnum,
      callback?,
      errCallback?
    ) => void;
    exec: (
      category: ProgramCategory,
      key: string,
      callback?: viewCallback
    ) => void;
    ipcRendererOn: (
      channel: string,
      func: (event: IpcRendererEvent, ...args: any[]) => void
    ) => void;
    ipcRendererSend: (channel: string, ...args: any[]) => void;
    getFileStatus: (keys?: string[], callback?: viewCallback) => void;
    getProgress: (keys?: string[], callback?: viewCallback) => void;
    sysInfo: (callback?: viewCallback) => void;
    regHandler: (action: string, name: string, handler: viewCallback) => void;
    loadInstallConfig: (callback?: viewCallback) => Promise;
    uninstallConfig: (key: string, callback?: viewCallback) => void;
    debuildEnvironment: (callback?: viewCallback) => void;
    getDebuildConfig: (callback?: viewCallback) => void;
    kylinWineDebuild: (
      bottle: string,
      data: any,
      callback?: viewCallback
    ) => void;
    createBackup: (data: any, callback?: viewCallback) => void;
    importBackup: (data: any, callback?: viewCallback) => void;
    openFolder: (
      target?: string,
      bottle?: string,
      openType?: Array<
        | "openFile"
        | "openDirectory"
        | "multiSelections"
        | "showHiddenFiles"
        | "createDirectory"
        | "promptToCreate"
        | "noResolveAliases"
        | "treatPackageAsDirectory"
        | "dontAddToRecent"
      >,
      callback?: viewCallback
    ) => void;
    openFolderChooseFile: (callback?: viewCallback) => void;
    getContainerStatus: (callback?: viewCallback) => void;
    wineKillAll: (callback?: viewCallback) => void;
    iconPathCompletion: (
      icon: string,
      exec: string,
      key: string,
      callback?: viewCallback
    ) => void;
    fileType: (path: string, callback?: viewCallback) => string;
    startWinExec: (data, callback?: viewCallback) => void;
    openDriverC: (bottle: string, callback?: viewCallback) => void;
    getWinFixDll: (callback?: viewCallback, errCallback?: viewCallback) => void;
    setWinFixDll: (
      dlls: string[],
      type: string,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    getContainerInfo: (callback?: viewCallback) => void;
    createShortcut: (
      bottle: string,
      data: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
    getDependencyList: (
      botttle: string,
      key: any,
      callback?: viewCallback
    ) => void;
    getInstalledSoftInfo: (name: string, callback?: viewCallback) => void;
    exportSoftConfig: (
      path: string,
      data: any,
      callback?: viewCallback,
      errCallback?: viewCallback
    ) => void;
  };
}

interface DownloadInfo {
  Key: string;
  File: string;
  Status: DownloadStatus;
  Progress: number;
}
/**
 * public Key: string = ""; //应用程序名称
  public File: string = ""; //对应的下载文件名
  public Status: DownloadStatus = DownloadStatus.Undownload;
    public Progress: number = 0.0; //下载百分比 0 - 100

 */
