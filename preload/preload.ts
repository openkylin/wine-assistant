/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { contextBridge, ipcRenderer } from "electron";
import { Notification } from "@arco-design/web-vue";
import {
  ActionEnum,
  DependencyCategory,
  IpcProtocol,
  IpcResult,
  OpDownloadParam,
  ParamInternal,
  ProgramCategory,
} from "../main/commons/define";

type viewCallback = (data: any) => void;

let portRenderer: MessagePort | undefined = undefined;
const callbacks: Map<string, viewCallback> = new Map<string, viewCallback>();
const errCallbacks: Map<string, viewCallback> = new Map<string, viewCallback>();
const pushHandler: Map<string, Map<string, viewCallback>> = new Map<
  string,
  Map<string, viewCallback>
>();

//contextBridge.exposeInMainWorld("acquireAssistantApi", acquireAssistantApi);
let myPostMessage = (
  cls: string,
  action: string,
  param: any,
  callback?: viewCallback,
  errCallback?: viewCallback
) => {
  acquire();
  let data = {} as IpcProtocol;
  data.Class = cls;
  data.Action = action;
  data.Data = param;
  if (callback != undefined) {
    callbacks.set(data.Class + data.Action, callback);
  }
  if (errCallback != undefined) {
    errCallbacks.set(data.Class + data.Action, errCallback);
  }
  portRenderer?.postMessage(data);
};
let acquire = () => {
  if (portRenderer == undefined) {
    const { port1, port2 } = new MessageChannel();
    ipcRenderer.postMessage("port", null, [port1]);
    portRenderer = port2;
    portRenderer.onmessage = (ev: MessageEvent<any>) => {
      let result = ev.data as IpcResult;
      if (result == null) {
        return;
      }
      if (result.Code < 0) {
        let ecb = errCallbacks.get(result.Class + result.Action);
        if (ecb) {
          ecb(result.Data);
        } else {
          Notification.error(result.Data);
        }
      } else {
        if (result.Class == "pushMessage") {
          pushHandler.get(result.Action)?.forEach((cb) => {
            cb(result.Data);
          });
        } else {
          let cb = callbacks.get(result.Class + result.Action);
          /*
          if (result.Action != "getProgress") {
            console.log(result);
          }
          */
          //console.log(result);
          if (cb != undefined) {
            cb(result.Data);
          }
        }
      }
    };
  }
};

contextBridge.exposeInMainWorld("assistantApi", {
  getSoftInfo: (callback?: viewCallback) => {
    myPostMessage("softInfo", "getSoftInfo", undefined, callback);
  },
  isLicense: () => {
    return false;
  },
  getPrograms: (callback?: viewCallback) => {
    myPostMessage("program", "getPrograms", undefined, callback);
  },
  getDependencies: (param: any, callback?: viewCallback) => {
    myPostMessage("dependency", "getDependencies", param, callback);
  },
  openDetail: (param: any, callback?: viewCallback) => {
    myPostMessage("program", "openDetail", param, callback);
  },
  openLicensePage: (param: any, callback?: viewCallback) => {
    myPostMessage("program", "openDetail", param, callback);
  },
  getLicenseUrl: (param: any, callback?: viewCallback) => {
    myPostMessage("program", "openLicense", param, callback);
  },
  recursiveProgramDeps: (
    category: string,
    key: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category as DependencyCategory;
    myPostMessage("program", "getDependencies", param, callback, errCallback);
  },
  getProgramDepsLicense: (
    category: string,
    key: string,
    bottle?: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category as DependencyCategory;
    param.Bottle = bottle;
    myPostMessage(
      "program",
      "getProgramDepsLicense",
      param,
      callback,
      errCallback
    );
  },
  ipcRendererSend: (channel: string, ...args: any[]) =>
    ipcRenderer.send(channel, ...args),
  ipcRendererOn: (
    channel: string,
    func: (event: IpcRendererEvent, ...args: any[]) => void
  ) => {
    ipcRenderer.on(channel, func);
  },

  install: (
    category: ProgramCategory,
    key: string,
    RealUrl?: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category;
    param.RealUrl = RealUrl;
    myPostMessage("programInternal", "install", param, callback, errCallback);
  },
  uninstall: (
    key: string,
    name: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Name = name;
    myPostMessage("programInternal", "uninstall", param, callback, errCallback);
  },
  exec: (
    category: ProgramCategory,
    key: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as ParamInternal;
    param.Key = key;
    param.Category = category;
    myPostMessage("programInternal", "exec", param, callback, errCallback);
  },
  opDownload: (
    appKey: string,
    fileKey: string,
    op: ActionEnum,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {} as OpDownloadParam;
    param.AppKey = appKey;
    param.FileKey = fileKey;
    param.Action = op;
    myPostMessage(
      "downloaderManager",
      "opDownload",
      param,
      callback,
      errCallback
    );
  },
  getFileStatus: (keys?: string[], callback?: viewCallback) => {
    let param = keys;
    myPostMessage("statManager", "getInstallStatus", param, callback);
  },
  uninstallConfig: (key: string, callback?: viewCallback) => {
    let param = key;
    myPostMessage("statManager", "uninstallConfig", param, callback);
  },
  getProgress: (keys?: string[], callback?: viewCallback) => {
    let param = keys;
    myPostMessage("downloaderManager", "getProgress", param, callback);
  },
  regHandler: (action: string, name: string, handler: viewCallback) => {
    if (!pushHandler.has(action)) {
      pushHandler.set(action, new Map<string, viewCallback>());
    }
    pushHandler.get(action)?.set(name, handler);
  },
  installDependencies: (
    keys: {}[],
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    myPostMessage("dependencyInternal", "install", keys, callback, errCallback);
  },
  sysInfo: (callback: viewCallback) => {
    myPostMessage("sysInfo", "getSysInfo", "", callback);
  },
  loadInstallConfig: (callback?: viewCallback) => {
    myPostMessage("program", "loadInstallConfig", undefined, callback);
  },
  debuildEnvironment: (callback?: viewCallback) => {
    myPostMessage("container", "debuildEnvironment", undefined, callback);
  },
  getDebuildConfig: (callback?: viewCallback) => {
    myPostMessage("container", "getDebuildConfig", undefined, callback);
  },
  kylinWineDebuild: (bottle: string, data: any, callback?: viewCallback) => {
    let param = {
      bottle: bottle,
      config: data,
    };
    myPostMessage("container", "kylinWineDebuild", param, callback);
  },
  createBackup: (data: any, callback?: viewCallback) => {
    let param = data;
    myPostMessage("container", "createBackup", param, callback);
  },
  importBackup: (data: any, callback?: viewCallback) => {
    let param = data;
    myPostMessage("container", "importBackup", param, callback);
  },
  openFolder: (
    target?: string,
    bottle?: string,
    openType?: Array<
      | "openFile"
      | "openDirectory"
      | "multiSelections"
      | "showHiddenFiles"
      | "createDirectory"
      | "promptToCreate"
      | "noResolveAliases"
      | "treatPackageAsDirectory"
      | "dontAddToRecent"
    >,
    callback?: viewCallback
  ) => {
    let param = {
      target: target,
      bottle: bottle,
      openType: openType,
    };
    myPostMessage("container", "openFolder", param, callback);
  },
  openFolderChooseFile: (callback?: viewCallback) => {
    myPostMessage("container", "openFolderChooseFile", undefined, callback);
  },
  iconPathCompletion: (
    icon: string,
    exec: string,
    key: string,
    callback?: viewCallback
  ) => {
    let param = {
      icon: icon,
      exec: exec,
      key: key,
    };
    myPostMessage("container", "iconPathCompletion", param, callback);
  },
  getContainerStatus: (callback?: viewCallback) => {
    myPostMessage("container", "getContainerStatus", undefined, callback);
  },
  wineKillAll: (callback?: viewCallback) => {
    myPostMessage("container", "wineKillAll", undefined, callback);
  },
  fileType: (path: string, callback?: viewCallback) => {
    let param = {
      path: path,
    };
    myPostMessage("container", "fileType", param, callback);
  },
  startWinExec: (data, callback?: viewCallback) => {
    let param = data;
    myPostMessage("container", "startExe", param, callback);
  },
  openDriverC: (bottle: string, callback?: viewCallback) => {
    let param = bottle;
    myPostMessage("container", "openDriverC", param, callback);
  },
  getWinFixDll: (callback?: viewCallback, errCallback?: viewCallback) => {
    myPostMessage(
      "container",
      "getWinFixDll",
      undefined,
      callback,
      errCallback
    );
  },
  setWinFixDll: (
    dlls: string[],
    type: string,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {
      dlls: dlls,
      type: type,
    };
    myPostMessage("container", "setWinFixDll", param, callback, errCallback);
  },
  getContainerInfo: (callback?: viewCallback) => {
    myPostMessage("container", "getContainerInfo", undefined, callback);
  },
  createShortcut: (
    bottle: string,
    data: any,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {
      bottle: bottle,
      config: data,
    };
    myPostMessage("container", "createShortcut", param, callback, errCallback);
  },
  getDependencyList: (bottle: string, key: any, callback?: viewCallback) => {
    let param = {
      bottle: bottle,
      key: key,
    };
    myPostMessage("dependency", "getDependencyList", param, callback);
  },
  getInstalledSoftInfo: (name: string, callback?: viewCallback) => {
    myPostMessage("container", "getInstalledSoftInfo", name, callback);
  },
  exportSoftConfig: (
    path: string,
    data: any,
    callback?: viewCallback,
    errCallback?: viewCallback
  ) => {
    let param = {
      path: path,
      data: data,
    };
    myPostMessage(
      "container",
      "exportSoftConfig",
      param,
      callback,
      errCallback
    );
  },
});
