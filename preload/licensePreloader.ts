/*
 * Copyright (C) 2023, KylinSoft Co., Ltd.All rights reserved.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 * Authors: xuewei <xuewei@kylinos.cn>
 * Authors: liangkeming <liangkeming@kylinos.cn>
 * Authors: linchaochao <linchaochao@kylinos.cn>
 * Authors: weizheng <weizheng@kylinos.cn>
 * Authors: daixiaoyu <daixiaoyu@kylinos.cn>
 *
 */
import { IpcRenderer, IpcRendererEvent, ipcRenderer } from "electron";

import { contextBridge } from "electron";

import { WorkDirConfig } from "../main/commons/config";

/*declare global {
	interface Window {
		ipcRenderer: IpcRenderer;
	}
} */
/*contextBridge.exposeInMainWorld('assistantApi2', {
	ipcRenderer: IpcRenderer;
}); */

/*if (process.contextIsolated) {
  try {
    contextBridge.exposeInMainWorld('electron', Electron)
//   contextBridge.exposeInMainWorld('api', api)
  } catch (error) {
    console.error(error)
  }
} else {
  // @ts-ignore (define in dts)
  window.electron = electronAPI
  // @ts-ignore (define in dts)
//  window.api = api
}*/

// 将ipcRenderer暴露给渲染进程
//contextBridge.exposeInMainWorld('ipcRenderer', ipcRenderer);
contextBridge.exposeInMainWorld("assistantApi", {
  ipcRendererOn: (
    channel: string,
    func: (event: IpcRendererEvent, ...args: any[]) => void
  ) => {
    ipcRenderer.on(channel, func);
  },
  ipcRendererSend: (channel: string, ...args: any[]) =>
    ipcRenderer.send(channel, ...args),
  onExternalUrl: (callback) =>
    ipcRenderer.on("webPopup", (event, data) => callback(data)),
  removeExternalUrlListener: (callback) =>
    ipcRenderer.removeListener("webPopup", callback),
  isLicense: () => {
    return true;
  },
});

console.log("keming preloader");
/*window.ipcRenderer.on("licenseUrl", (event: IpcRendererEvent, data: any) => {
  console.log('keming接受'+data);
});*/
